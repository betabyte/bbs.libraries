﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Contracts
{
  public interface IDataRepository
  {
  }
  public interface IDataRepository<T> : IDataRepository
    where T : class, IIdentifiableEntity, new()
  {
    T Add(T entity);

    void Remove (T entity);
    void Remove(Func<T, bool> queryToRun);

    T Update (T entity);

    IEnumerable<T> Get();
    T Get(Func<T, bool> querytToRun);
    IEnumerable<T> GetEntities(Func<T, bool> queryToRun);
  }
}
