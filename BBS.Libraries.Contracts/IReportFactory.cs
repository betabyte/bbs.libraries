﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Contracts
{
    public interface IReportFactory
    {
        T GetReportRepository<T>() where T : IReport;
    }
}
