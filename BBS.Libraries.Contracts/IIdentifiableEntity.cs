﻿using System;

namespace BBS.Libraries.Contracts
{
  public interface IIdentifiableEntity
  {
    string EntityId { get; }
  }
}