﻿using System;
using System.Data;
using BBS.Libraries.Contracts;
using BBS.Libraries.Extensions;

namespace BBS.Libraries.SQL.MySQL
{
  public class DataReader : BBS.Libraries.Contracts.IDataReader
  {
    public void Dispose()
    {
      throw new Exception();
    }

    public string GetName(int i)
    {
      return _reader.GetName(i);
    }

    public string GetDataTypeName(int i)
    {
      return _reader.GetDataTypeName(i);
    }

    public Type GetFieldType(int i)
    {
      return _reader.GetFieldType(i);
    }

    public object GetValue(int i)
    {
      return _reader.GetValue(i);
    }

    public int GetValues(object[] values)
    {
      return _reader.GetValues(values);
    }

    public int GetOrdinal(string name)
    {
      return _reader.GetOrdinal(name);
    }

    public bool GetBoolean(int i)
    {
      return _reader.GetBoolean(i);
    }

    public byte GetByte(int i)
    {
      return _reader.GetByte(i);
    }

    public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
    {
      return _reader.GetBytes(i, fieldOffset, buffer, bufferoffset, length);
    }

    public char GetChar(int i)
    {
      return _reader.GetChar(i);
    }

    public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
    {
      return _reader.GetChars(i, fieldoffset, buffer, bufferoffset, length);
    }

    public Guid GetGuid(int i)
    {
      return _reader.GetGuid(i);
    }

    public short GetInt16(int i)
    {
      return _reader.GetInt16(i);
    }

    public int GetInt32(int i)
    {
      return _reader.GetInt32(i);
    }

    public long GetInt64(int i)
    {
      return _reader.GetInt64(i);
    }

    public float GetFloat(int i)
    {
      return _reader.GetFloat(i);
    }

    public double GetDouble(int i)
    {
      return _reader.GetDouble(i);
    }

    public string GetString(int i)
    {
      return _reader.GetString(i);
    }

    public decimal GetDecimal(int i)
    {
      return _reader.GetDecimal(i);
    }

    public DateTime GetDateTime(int i)
    {
      return _reader.GetDateTime(i);
    }

    public System.Data.IDataReader GetData(int i)
    {
      return _reader.GetData(i);
    }

    public bool IsDBNull(int i)
    {
      return _reader.IsDBNull(i);
    }

    public int FieldCount { get; private set; }

    object IDataRecord.this[int i]
    {
      get { return _reader[i]; }
    }

    object IDataRecord.this[string name]
    {
      get { return _reader[name]; }
    }

    public void Close()
    {
      _reader.Close();
    }

    public DataTable GetSchemaTable()
    {
      throw new NotImplementedException();
    }

    public bool NextResult()
    {
      return _reader.NextResult();
    }

    public bool Read()
    {
      return _reader.Read();
    }

    public int Depth { get { return _reader.Depth; } }
    public bool IsClosed { get{return _reader.IsClosed;} }
    public int RecordsAffected { get { return _reader.RecordsAffected; } }
    
    public string GetName(string target)
    {
      return _reader.GetName(GetOrdinal(target));
    }

    public Type GetFieldType(string target)
    {
      return _reader.GetFieldType(target);
    }

    public bool GetBoolean(string target)
    {
      return _reader.GetBoolean(target);
    }

    public byte[] GetBytes(string target)
    {
      var result = _reader.GetValue(GetOrdinal(target));

      if (result is byte[])
      {
        return result as byte[];
      }
      return null;
    }

    public DateTime GetDateTime(string target)
    {
      return _reader.GetDateTime(target);
    }

    public DateTime? GetDateTimeNullable(string target)
    {
      return _reader.GetString(target).ToDateTimeNullable();
    }

    public Guid GetGuid(string target)
    {
      return _reader.GetGuid(target);
    }

    public Guid? GetGuidNullable(string target)
    {
      return _reader.GetString(target).ToGuidNullable();
    }

    public int GetInt(string target)
    {
      return _reader.GetInt32(target);
    }

    public int? GetIntNullable(string target)
    {
      return _reader.GetString(target).ToIntNullable();
    }

    public float GetFloat(string target)
    {
      return _reader.GetFloat(target);
    }

    public float? GetFloatNullable(string target)
    {
      return null;
    }

    public double GetDouble(string target)
    {
      return _reader.GetDouble(target);
    }

    public double? GetDoubleNullable(string target)
    {
      throw new NotImplementedException();
      //return _reader.GetDouble(target).ToDoubleNullable();
    }

    public string GetString(string target)
    {
      return IsDbNull(target) ? string.Empty : _reader.GetString(target);
    }

    public bool IsDbNull(string target)
    {
      return _reader.IsDBNull(GetOrdinal(target));
    }

    public T GetEnum<T>(string target)
    {
      return BBS.Libraries.Enums<T>.Parse(_reader.GetString(target));
    }

    public MySql.Data.MySqlClient.MySqlDataReader _reader;
  }
}