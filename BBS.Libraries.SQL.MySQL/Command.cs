﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;
using MySql.Data.MySqlClient;

namespace BBS.Libraries.SQL.MySQL
{
  public partial class Command : BBS.Libraries.DB.Command
  {
    private void GetParameterCollection()
    {
      foreach (var i in base._parameters)
      {
          if (i.Value.Type == DB.ParameterType.GuidNullable || 
              i.Value.Type == DB.ParameterType.NumericNullable || 
              i.Value.Type == DB.ParameterType.String)
          {
              if (i.Value.Object == null || string.IsNullOrWhiteSpace(i.Value.Object as string))
              {
                  i.Value.Object = System.DBNull.Value;
              }
          }

        var param = _command.CreateParameter();
        param.ParameterName = i.Key.ParameterName;
        param.MySqlDbType = i.Value.Type.ToMySqlDbType();
        param.Value = i.Key.Value;

        var paramterName = param.ParameterName;
        _command.Parameters.AddWithValue(paramterName, param.Value);
      }
    }

    private MySql.Data.MySqlClient.MySqlCommand _command;

    private void PrepareCommand()
    {
      if (_reader == null || _reader.IsClosed)
      {
        _reader = new DataReader();
        GetParameterCollection();
        _command.Prepare();
        
        _command.CommandText = base._Command.ToString();
      }
    }

    public Command(string connectionString)
    {
      base.Connection = new MySqlConnection();
      base.Connection.ConnectionString = connectionString;
      
      MySqlDataReader _reader = null;
      
      Open();
    }

    private IDataReader _reader;
    
    public override IDataReader Reader 
    {
      get
      {
        return _reader;
      }
      set
      {
        
      }
    }

    public bool ExecuteReader()
    {
        try
        {
            PrepareCommand();
            (_reader as DataReader)._reader = _command.ExecuteReader();
            return Reader != null;
        }
        catch (MySqlException msql)
        {
            throw new System.Data.DataException(base.SqlErrorString, msql);
        }
        catch (System.Exception exception)
        {
            throw new System.Exception("General Sql Exception", exception);
        }
    }

    public override void Open()
    {
      _command = Connection.CreateCommand() as MySqlCommand;
      base.Connection.Open();
    }

    public new int ExecuteNonQuery()
    {
        try
        {
            PrepareCommand();
            return _command.ExecuteNonQuery();
        }
        catch (MySqlException msql)
        {
            throw new System.Data.DataException(base.SqlErrorString, msql);
        }
        catch (System.Exception exception)
        {
            throw new System.Exception("General Sql Exception", exception);
        }
    }

    public override string ToString()
    {
      return base.ToString();
    }
  }
}
