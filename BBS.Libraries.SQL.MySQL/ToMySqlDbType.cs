﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;
using MySql.Data.MySqlClient;


namespace BBS.Libraries.SQL.MySQL
{
  public static partial class Extensions
  {
    public static MySqlDbType ToMySqlDbType(this BBS.Libraries.DB.ParameterType parameter)
    {
      switch (parameter)
      {
        case DB.ParameterType.Numeric:
        case DB.ParameterType.NumericArray:
        case DB.ParameterType.NumericNullable:
          return MySqlDbType.Float;
        case DB.ParameterType.Guid:
        case DB.ParameterType.GuidNullable:
        case DB.ParameterType.GuidArray:
          return MySqlDbType.Guid;
        case DB.ParameterType.StringArray:
        case DB.ParameterType.String:
          return MySqlDbType.VarChar;
        case DB.ParameterType.Bit:
          return MySqlDbType.Bit;
        case DB.ParameterType.Blob:
          return MySqlDbType.Blob;
        default:
          return MySqlDbType.VarChar;
      }
    }
  }
}
