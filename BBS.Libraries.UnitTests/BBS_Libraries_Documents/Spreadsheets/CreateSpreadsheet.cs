﻿using System.IO;
using NUnit.Framework;

namespace BBS.Libraries.UnitTests.BBS_Libraries_Documents.Spreadsheets
{
  [TestFixture]
  public class CreateSpreadsheet
  {
    [Test]
    public void Execute()
    {
      using (var memstr = new BBS.Libraries.IO.File.FileStream(@"c:\temp\tester.xlsx", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
      {
        using (var spreadsheet = new BBS.Libraries.Documents.Spreadsheets())
        {
          var sheet = spreadsheet.Book.AddWorksheet("SheetName");

          var cell = spreadsheet.Book.Cells;

          var headerStyle = new BBS.Libraries.Documents.Spreadsheets.Style();

          headerStyle.Alignment.Horizontal = Documents.Spreadsheets.AlignmentHorizontal.Right;
          headerStyle.Font.Color = System.Drawing.Color.Black;
          headerStyle.Font.Bold = true;
          headerStyle.Font.Italic = false;
          headerStyle.Font.Underline = Documents.Spreadsheets.FontUnderLines.None;

          var accData = new BBS.Libraries.Documents.Spreadsheets.Style();
          accData.Alignment.Horizontal = Documents.Spreadsheets.AlignmentHorizontal.Center;
          accData.Font.Color = System.Drawing.Color.Black;
          accData.Font.Bold = false;
          accData.Font.Italic = false;
          accData.Font.Underline = Documents.Spreadsheets.FontUnderLines.None;
          accData.CellFormat.Format = "$ #,##0.00";

          var percentage = new BBS.Libraries.Documents.Spreadsheets.Style();
          percentage.Alignment.Horizontal = Documents.Spreadsheets.AlignmentHorizontal.Center;
          percentage.Font.Color = System.Drawing.Color.Red;
          percentage.Font.Bold = false;
          percentage.Font.Italic = false;
          percentage.Font.Underline = Documents.Spreadsheets.FontUnderLines.None;
          percentage.CellFormat.Format = "0.00%";
          percentage.Fill.BackgroundColor = System.Drawing.Color.Black;
          percentage.Fill.PatternType = Documents.Spreadsheets.FillPattern.Solid;
          percentage.Border.BottomBorder = Documents.Spreadsheets.BorderStyle.Double;
          percentage.Border.LeftBorder = Documents.Spreadsheets.BorderStyle.Medium;


          cell[0, 0].SetValue("Header 1");
          cell[0, 0].SetStyle(headerStyle);

          cell[0, 1].SetValue("Header 2");
          cell[0, 1].SetStyle(headerStyle);

          cell[0, 2].SetValue("Header 3");
          cell[0, 2].SetStyle(headerStyle);

          for (int i = 0; i < 10; i++)
          {
            cell[1, i].SetValue<double>(123456.789);
            cell[1, i].SetStyle(accData);
          }

          for (int i = 0; i < 10; i++)
          {
            cell[2, i].SetValue<double>(0.22);
            cell[2, i].SetStyle(percentage);
          }
          sheet.Protect();
          sheet.Columns().AdjustToContents();

          //spreadsheet.Book.SaveAs(@"C:\temp\tester.xlsx", false);
          spreadsheet.Book.SaveAs(memstr.Stream);
        }
      }
    }
  }
}
