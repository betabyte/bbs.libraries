﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Emails;
using BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Models;
using NUnit.Framework;

namespace BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs
{
    [TestFixture]
    public class PdfGeneratorsTests
    {
      [Test]
      public void CreateGenerator()
      {
        var items = new List<BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Models.LineItem>()
        {
          new LineItem(){ Name = "Hourly Rate", Price = 75d, Quantity = 20 },
          new LineItem(){ Name = "L1 Response", Price = 1500d, Quantity = 12 },
          new LineItem(){ Name = "Hosting - Yearly", Price = 900, Quantity = 1 },
          new LineItem(){ Name = "Consulting", Price = 150, Quantity = 20 }
        };

        var model = new BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Models.TestPdfsModel()
        {
          Name = "Brenton Lamar",
          Today = DateTime.Today,
          LineItems = items,
          IssueDate = DateTime.Today,
          DueDate = DateTime.Today.AddDays(15)
        };
        var fileOutput = @"c:\temp\test.pdf";
        
        new BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Generators.TestGenerator().TestPdf(model, fileOutput);
      }
    }
}
