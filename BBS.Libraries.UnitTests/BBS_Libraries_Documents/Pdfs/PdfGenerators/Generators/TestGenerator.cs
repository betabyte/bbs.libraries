﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Generators
{
  public class TestGenerator 
  {
    public void TestPdf(BBS_Libraries_Documents.Pdfs.PdfGenerators.Models.TestPdfsModel testPdfsModel, string fileLocation)
    {
      var generator = new BBS.Libraries.Documents.PDF.RazorContentGenerator<BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Models.TestPdfsModel>(
        BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Generators.T4Files.RepositryNamespace,
        BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Generators.T4Files.Views.TestEmails.Output_cshtml,
        fileLocation
      );
      
      generator.Generate(testPdfsModel);
    }
  }
}
