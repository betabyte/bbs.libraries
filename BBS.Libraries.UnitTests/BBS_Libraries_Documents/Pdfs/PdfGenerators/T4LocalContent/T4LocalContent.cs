﻿using System.CodeDom.Compiler;
using System.Diagnostics;

#region BBS.Services.EmailGenerators.GAS.T4Files


namespace BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Generators
{
  // This class typically generated via t4
  public static partial class T4Files
  {
    public static partial class Views
    {
      //public static readonly string _root = "~/BBS.Services.EmailGenerators.GAS/Views/";

      public static partial class TestEmails
      {
        public static readonly string _root = "~/BBS_Libraries_Documents/Pdfs/PdfGenerators/Views/TestPdfs/";
        public static readonly string Output_cshtml = "~/BBS_Libraries_Documents/Pdfs/PdfGenerators/Views/TestPdfs/Output.cshtml";
      }
    }
    public static string RepositryNamespace = "BBS.Libraries.UnitTests";

  }
}
#endregion