﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;


namespace BBS.Libraries.UnitTests.BBS_Libraries_Documents.Pdfs.PdfGenerators.Models
{
  public class TestPdfsModel : IRazorContentModel
  {
    public string Name { get; set; }
    public DateTime Today { get; set; }

    public List<LineItem> LineItems { get; set; }

    public double Subtotal { get { return this.LineItems.Sum(x=>( x.Price * x.Quantity)); } }
    public double Tax { get { return this.Subtotal * .07d; } }
    public double Total { get { return this.Subtotal + this.Tax; } }

    public DateTime IssueDate { get; set; }
    public DateTime DueDate { get; set; }
  }

  public class LineItem
  {
    public string Name { get; set; }
    public double Price { get; set; }
    public int Quantity { get; set; }
  }
}
