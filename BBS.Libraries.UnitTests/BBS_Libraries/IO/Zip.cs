﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace BBS.Libraries.UnitTests.BBS_Libraries.IO
{
  [TestFixture]
  public class Zip
  {
    [Test]
    public void Execute()
    {
      // Come up and create a few file streams

      var files = new List<BBS.Libraries.IO.File.FileStream>();
      files.Add(new Libraries.IO.File.FileStream(@"c:\temp\tester\drop case.sql"));
      files.Add(new Libraries.IO.File.FileStream(""));

      BBS.Libraries.IO.Zip.ZipFile(@"c:\temp\tester\zipfile.zip", files);
    }
  }
}
