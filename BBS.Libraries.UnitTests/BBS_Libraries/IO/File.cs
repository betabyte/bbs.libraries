﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Extensions;
using NUnit.Framework;

namespace BBS.Libraries.UnitTests.BBS_Libraries.IO
{
  [TestFixture]
  public class File
  {
    [Test]
    public void CreateLocalFile()
    {
      // Local file
      var file = new BBS.Libraries.IO.File.FileStream("");

    }

    [Test]
    public void GetFtpFile()
    {
      // Local file
      var file = new BBS.Libraries.IO.File.FileStream("");

      Assert.IsTrue(file.LocationType.Equals(BBS.Libraries.IO.File.FileLocationType.FTP));

      BBS.Libraries.IO.File.WriteFile(BBS.Libraries.IO.File.CombinePathAndFileName(@"c:\temp\tester", "tester.txt.gz"), file.Stream.ToByteArray(), true);
    }

    [Test]
    public void GetLocalFile()
    {
      var file = new BBS.Libraries.IO.File.FileStream(@"C:\Temp\tester\tester.txt.gz");

      Assert.IsTrue(file.LocationType.Equals(BBS.Libraries.IO.File.FileLocationType.Local));

      BBS.Libraries.IO.File.WriteFile(BBS.Libraries.IO.File.CombinePathAndFileName(@"c:\temp\tester", "tester.txt.gz"), file.Stream.ToByteArray(), true);

      Assert.IsTrue(System.IO.File.Exists(@"c:\temp\tester\tester.txt.1.gz"));
    }

    [Test]
    public void GetHttpFile()
    {
      var file = new BBS.Libraries.IO.File.FileStream("http://www.betabytesoftware.com/Content/images/weblogo.png");

      Assert.IsTrue(file.LocationType.Equals(BBS.Libraries.IO.File.FileLocationType.Http));

      BBS.Libraries.IO.File.WriteFile(BBS.Libraries.IO.File.CombinePathAndFileName(@"c:\temp\tester", "weblogo-downloaded.png"), file.Stream.ToByteArray(), true);

      Assert.IsTrue(System.IO.File.Exists(@"c:\temp\tester\weblogo.png"));

      System.IO.File.Delete(BBS.Libraries.IO.File.CombinePathAndFileName(@"c:\temp\tester", "weblogo-downloaded.png"));
    }
  }
}
