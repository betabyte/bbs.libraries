﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Emails;
using BBS.Libraries.Extensions;
using BBS.Libraries.UnitTests.BBS_Libraries.Emails;
using NUnit.Framework;

namespace BBS.Libraries.UnitTests.BBS_Libraries
{
    [TestFixture]
    public class EmailGeneratorsTests
    {
      [Test]
      public void Create()
      {
        var mhtmlViewAlternateView = AlternateView.CreateAlternateViewFromString("\r\n\r\n<h3>Hello, Godzilla!</h3>\r\n\r\n<p>Please find the enclosed documents to fulfill your request.</p>\r\n\r\n<p>Don't bother us again</p>\r\n\r\n<p>Sincerely, <br />BBS</p>", new ContentType("text/html"));
        var plainViewAlternateView = AlternateView.CreateAlternateViewFromString("Hello, Godzilla!\r\n\r\nPlease find the enclosed documents to fulfill your request.\r\nDon't bother us again\r\n\r\nSincerely,\r\nBBS");
        var mm = new BBS.Libraries.Emails.MailMessage();
        mm.Subject = "Please Stop Bothering Us";
        mm.AlternateViews = new MailMessageAlternateViewCollection(){ plainViewAlternateView, mhtmlViewAlternateView };
        mm.To = new EmailAddressCollection(new []{"godzilla@monsters.inc"});
        mm.From = new EmailAddress("support@bbs.com");
        mm.CC = new EmailAddressCollection();
        mm.Bcc = new EmailAddressCollection();
        mm.Attachments = new MailMessageAttachmentCollection();
        mm.Priority = MailPriority.High;

        var model = new BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Models.TestEmails.TestEmailsModel()
        {
          Name = "Godzilla!",
          ToEmailAddressCollection = new EmailAddressCollection(new[] { "godzilla@monsters.inc" }),
          FromEmailAddress = new EmailAddress("support@bbs.com")
        };

        var message = new BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators.TestGenerator().TestMailMessage(model);
        
        Assert.AreEqual(message.To.Count, mm.To.Count);
        for (int i = 0; i < message.To.Count; i++)
        {
          Assert.AreEqual(message.To[i].Value, mm.To[i].Value);
        }

        Assert.AreEqual(message.CC.Count, mm.CC.Count);
        for (int i = 0; i < message.CC.Count; i++)
        {
          Assert.AreEqual(message.CC[i].Value, mm.CC[i].Value);
        }

        Assert.AreEqual(message.Bcc.Count, mm.Bcc.Count);
        for (int i = 0; i < message.Bcc.Count; i++)
        {
          Assert.AreEqual(message.Bcc[i].Value, mm.Bcc[i].Value);
        }

        Assert.AreEqual(message.AlternateViews.Count, mm.AlternateViews.Count);
        for (int i = 0; i < message.AlternateViews.Count; i++)
        {
          Assert.AreEqual(message.AlternateViews[i].Content, mm.AlternateViews[i].Content);
          Assert.AreEqual(message.AlternateViews[i].ContentType, mm.AlternateViews[i].ContentType);
        }

        Assert.AreEqual(message.Attachments.Count, mm.Attachments.Count);
        for (int i = 0; i < message.Attachments.Count; i++)
        {
          Assert.AreEqual(message.Attachments[i].Content, mm.Attachments[i].Content);
          Assert.AreEqual(message.Attachments[i].ContentType, mm.Attachments[i].ContentType);
        }

        Assert.AreEqual(message.Priority, mm.Priority);
        Assert.AreEqual(message.Subject, mm.Subject);

      }

      [Test]
      public void CreateGenerator()
      {
        var model = new BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Models.TestEmails.TestEmailsModel()
        {
          Name = "Brenton Lamar",
          ToEmailAddressCollection = new EmailAddressCollection(new []{"brenton@hcowz.com"}),
          FromEmailAddress = new EmailAddress("support@bbs.com")
        };

        var mailMessage = new BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators.TestGenerator().TestMailMessage(model);

        mailMessage.Attachments.Add(new Attachment(@"C:\Temp\test.pdf"));

        // Serialize and save it

        var serializer = new BBS.Libraries.IO.Serializers.XmlSerializer<BBS.Libraries.Emails.MailMessage>();
        serializer.Serialize(mailMessage);
      
        serializer.Write(mailMessage, @"c:\temp\test.xml");
      }

      [Test]
      public void DeserializeAndSend()
      {
        var serializer = new BBS.Libraries.IO.Serializers.XmlSerializer<BBS.Libraries.Emails.MailMessage>();
        var x = serializer.DeserializeFile(@"c:\temp\test.xml");

        var pickupDirectory = @"c:\temp\emails";
        if(!System.IO.Directory.Exists(pickupDirectory))
        {
          System.IO.Directory.CreateDirectory(pickupDirectory);
        }
      
        using (var smtp = new SmtpClient(){DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory,PickupDirectoryLocation = pickupDirectory})
        {
          smtp.Send(x.Message());
        }
      }

      [Test]
      public void CreateAndSend()
      {
        var model = new BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Models.TestEmails.TestEmailsModel()
        {
          Name = "Godzilla!",
          ToEmailAddressCollection = new EmailAddressCollection(new []{"godzilla@monsters.inc"}),
          FromEmailAddress = new EmailAddress("support@bbs.com")
        };

        var message = new BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators.TestGenerator().TestMailMessage(model);
        
        using (var smtp = new SmtpClient("127.0.0.1"))
        {
          smtp.Send(message.Message());
        }
      }
    }
}
