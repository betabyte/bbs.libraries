﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators
{
  public class TestGenerator : BBS.Libraries.Emails.EmailGenerator
  {
    public BBS.Libraries.Emails.MailMessage TestMailMessage(BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Models.TestEmails.TestEmailsModel testEmailsModel)
    {
      var generator = new BBS.Libraries.Emails.RazorContentGenerator<BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Models.TestEmails.TestEmailsModel>(
        BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators.T4Files.RepositryNamespace,
        BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators.T4Files.Views.TestEmails.SubjectWeeklyReport_cshtml,
        BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators.T4Files.Views.TestEmails.PlainTextWeeklyReport_cshtml,
        BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators.T4Files.Views.TestEmails.MhtmlWeeklyReport_cshtml
        );

      return generator.Generate(testEmailsModel);
    }
  }
}
