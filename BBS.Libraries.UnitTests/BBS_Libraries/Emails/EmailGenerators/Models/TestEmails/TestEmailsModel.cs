﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;
using BBS.Libraries.Emails;

namespace BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Models.TestEmails
{
  public class TestEmailsModel : IEmailBaseModel
  {
    public EmailAddress FromEmailAddress { get; set; }
    public EmailAddressCollection ToEmailAddressCollection { get; set; }
    public EmailAddressCollection CcEmailAddressCollection { get; set; }
    public EmailAddressCollection BccEmailAddressCollection { get; set; }
    public MailMessageAttachmentCollection Attachments { get { return null; } }
    public MailPriority Priority { get { return MailPriority.High; } }

    public string Name { get; set; }
  }
}
