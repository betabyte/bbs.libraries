﻿using System.CodeDom.Compiler;
using System.Diagnostics;

#region BBS.Services.EmailGenerators.GAS.T4Files


namespace BBS.Libraries.UnitTests.BBS_Libraries.Emails.EmailGenerators.Generators
{
  // This class typically generated via t4
  public static partial class T4Files
  {
    public static partial class Views
    {
      //public static readonly string _root = "~/BBS.Services.EmailGenerators.GAS/Views/";

      public static partial class TestEmails
      {
        public static readonly string _root = "~/BBS_Libraries/Emails/EmailGenerators/Views/TestEmails/";
        public static readonly string MhtmlWeeklyReport_cshtml = "~/BBS_Libraries/Emails/EmailGenerators/Views/TestEmails/MhtmlTestEmails.cshtml";
        public static readonly string PlainTextWeeklyReport_cshtml = "~/BBS_Libraries/Emails/EmailGenerators/Views/TestEmails/PlainTextTestEmails.cshtml";
        public static readonly string SubjectWeeklyReport_cshtml = "~/BBS_Libraries/Emails/EmailGenerators/Views/TestEmails/SubjectTestEmails.cshtml";
      }
    }
    public static string RepositryNamespace = "BBS.Libraries.UnitTests";

  }
}
#endregion