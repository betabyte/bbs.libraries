﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Emails;
using NUnit.Framework;

namespace BBS.Libraries.UnitTests.BBS_Libraries.Emails
{
  [TestFixture]
  public class Entities
  {
    private static string ValidEmailAddress = "brenton@helpfulcowz.com";
    private static string InvalidEmailAddress = "none@none123";
    private static BBS.Libraries.Log.StatusTracker statusTracker = new BBS.Libraries.Log.StatusTracker();

    [Test]
    public void ValidateEmail()
    {
      var goodEmailAddresses = new List<string>()
      {
        "email@example.com",
        "firstname.lastname@example.com",
        "email@subdomain.example.com",
        "firstname+lastname@example.com",
        "email@123.123.123.123",
        "email@[123.123.123.123]",
        "1234567890@example.com",
        "email@example-one.com",
        "_______@example.com",
        "email@example.name",
        //"email@example.museum",
        "email@example.co.jp",
        "firstname-lastname@example.com"
      };
      var badEmailAddresses = new List<string>()
      {
        "plainaddress",
        "#@%^%#$@#$@#.com",
        "@example.com",
        "Joe Smith <email@example.com>",
        "email.example.com",
        "email@example@example.com",
        ".email@example.com",
        "email.@example.com",
        "email..email@example.com",
        "あいうえお@example.com",
        "email@example.com (Joe Smith)",
        "email@example",
        "email@-example.com",
        "email@example.web",
        "email@111.222.333.44444",
        "email@example..com",
        "Abc..123@example.com",
      };

      statusTracker.AddToLog("Creating new emails for testing");
      
      foreach (var item in goodEmailAddresses)
      {
        var e = new BBS.Libraries.Emails.EmailAddress(item);
        Assert.IsTrue(e.IsValid);
      }
      
      statusTracker.AddToLog("Good Emails Passed");

      foreach (var item in badEmailAddresses)
      {
        var e = new BBS.Libraries.Emails.EmailAddress(item);
        Assert.IsFalse(e.IsValid);
      }
      
      statusTracker.AddToLog("Bad Emails Passed");
    }
    [Test]
    public void SerializeEmailMessage()
    {
      var emailMessage = new BBS.Libraries.Emails.MailMessage()
      {
        From = new EmailAddress("brenton@helpfulcowz.com"),
        To = new EmailAddressCollection(new []{"test@test.net"}),
        Subject = "Hello, this is a test",
        Bcc = new EmailAddressCollection(new []{"bcc@bcc.address.com"}),
        Body = "This is a test body",
        DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess,
        IsBodyHtml = false,
        Sender = new EmailAddress("test@tester.net"),
        Priority = MailPriority.High
      };

      var emailMessageToSerialize = new BBS.Libraries.Emails.MailMessage()
      {
        From = new EmailAddress("brenton@helpfulcowz.com"),
        To = new EmailAddressCollection(new[] { "test@test.net" }),
        Subject = "Hello, this is a test",
        Bcc = new EmailAddressCollection(new[] { "bcc@bcc.address.com" }),
        Body = "This is a test body",
        DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess,
        IsBodyHtml = false,
        Sender = new EmailAddress("test@tester.net"),
        Priority = MailPriority.High
      };
      
      statusTracker.AddToLog("Created new items");
      
      // Serialize the object first, deserialize and compare
      var serializer = new BBS.Libraries.IO.Serializers.XmlSerializer<BBS.Libraries.Emails.MailMessage>();
      var message = serializer.Serialize(emailMessageToSerialize);

      var deSerializer = new BBS.Libraries.IO.Serializers.XmlSerializer<BBS.Libraries.Emails.MailMessage>();
      var serializedThenDeserialized = deSerializer.Deserialize(message);
      
      statusTracker.AddToLog("Serialized and De-serialized");

      AssertEx.PropertyValuesAreEquals(emailMessage, emailMessageToSerialize);
      statusTracker.AddToLog("Both initial objects are equal");

      AssertEx.PropertyValuesAreEquals(emailMessageToSerialize, serializedThenDeserialized);
      statusTracker.AddToLog("Serialization happened correctly");

      AssertEx.PropertyValuesAreEquals(emailMessage, serializedThenDeserialized);
      statusTracker.AddToLog("De-Serialization happened correctly");
    }
  }

  public static class AssertEx
  {
    private static List<Type> _systemTypes;

    private static List<Type> SystemTypes 
    { 
      get 
      { 
        if(_systemTypes == null)
        {
          _systemTypes = Assembly.GetExecutingAssembly().GetType().Module.Assembly.GetExportedTypes().ToList();
        }
        return _systemTypes;
      }
    }

    public static void PropertyValuesAreEquals(object actual, object expected)
    {
      PropertyInfo[] properties = expected.GetType().GetProperties();
      foreach (PropertyInfo property in properties)
      {
        object expectedValue = property.GetValue(expected, null);
        object actualValue = property.GetValue(actual, null);

        if (actualValue is IList)
        {
          AssertListsAreEquals(property, (IList)actualValue, (IList)expectedValue);
        }
        else if (actualValue != null && !SystemTypes.Contains(actualValue.GetType()))
        {
          PropertyValuesAreEquals(actualValue, expectedValue);
        }
        else if (!Equals(expectedValue, actualValue))
        {
          Assert.Fail("Property {0}.{1} does not match. Expected: {2} but was: {3}", property.DeclaringType.Name, property.Name, expectedValue, actualValue);
        }
      }
    }

    private static void AssertListsAreEquals(PropertyInfo property, IList actualList, IList expectedList)
    {
      if (actualList.Count != expectedList.Count)
        Assert.Fail("Property {0}.{1} does not match. Expected IList containing {2} elements but was IList containing {3} elements", property.PropertyType.Name, property.Name, expectedList.Count, actualList.Count);

      for (int i = 0; i < actualList.Count; i++)
      {
        if (!SystemTypes.Contains(actualList[i]))
        {
          PropertyValuesAreEquals(actualList[i], expectedList[i]);
        }
        else if (!Equals(actualList[i], expectedList[i])) 
        { 
          Assert.Fail("Property {0}.{1} does not match. Expected IList with element {1} equals to {2} but was IList with element {1} equals to {3}", property.PropertyType.Name, property.Name, expectedList[i], actualList[i]);
        }
      }
    }
  }
}
