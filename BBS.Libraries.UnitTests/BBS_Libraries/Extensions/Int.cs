﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Extensions;
using NUnit.Framework;

namespace BBS.Libraries.UnitTests.BBS_Libraries.Extensions
{
  [TestFixture]
  public class Int
  {
    private static BBS.Libraries.Log.StatusTracker statusTracker = new BBS.Libraries.Log.StatusTracker();

    [Test]
    public void ToBoolZeroOrOne()
    {
      var falseItem = 0;
      var trueItem = 1;

      statusTracker.AddToLog(string.Format("{0} is considered {1}", falseItem, falseItem.ToBoolean()));
      statusTracker.AddToLog(string.Format("{0} is considered {1}", trueItem, trueItem.ToBoolean()));
      
      Assert.IsFalse(falseItem.ToBoolean());
      Assert.IsTrue(trueItem.ToBoolean());
    }

    [Test]
    public void ToBoolWithLists()
    {
      var trueValues = new int[]{0, 1, 2};
      var falseValues = new int[]{-1, -2, -3};

      for (int i = -3; i < 3; i++)
      {
        if (i < 0)
        {
          Assert.IsFalse(i.ToBoolean(trueValues, falseValues));
        }
        else
        {
          Assert.IsTrue(i.ToBoolean(trueValues, falseValues));
        }
      }
    }

    [Test]
    public void ToDouble()
    {
      var item = 23;

      var doubleItem = 23.0d;

      Assert.AreEqual(item.ToDouble(), doubleItem);
    }
  }
}
