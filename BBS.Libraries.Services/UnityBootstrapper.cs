﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace BBS.Libraries.Services
{
    public static class UnityBootstrapper
    {
        public static IUnityContainer Initialize(IUnityContainer container)
        {
            BuildUnityContainer(container);

            return container;
        }

        private static void BuildUnityContainer(IUnityContainer container)
        {
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            var configured = section.Configure(container);
        }
    }
}
