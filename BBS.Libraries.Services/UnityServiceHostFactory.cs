﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System.Configuration;

namespace BBS.Libraries.Services
{
    public class UnityServiceHostFactory : System.ServiceModel.Activation.ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            var serviceHost = new UnityServiceHost(serviceType, baseAddresses);
            var container = new UnityContainer();
            serviceHost.Container = container;

            //configure container
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");

            section.Containers.Default.Configure(serviceHost.Container);
            return serviceHost;
        }
    }
}
