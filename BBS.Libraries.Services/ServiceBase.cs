﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;


namespace BBS.Libraries.Services
{
    public class ServiceBase : BBS.Libraries.Contracts.IServiceBase
    {
        public ServiceBase() { }

        public T ExecuteSafely<T>(Func<T> stuffToDo)
        {
            try
            {
                return stuffToDo.Invoke();
            }
            catch (FaultException fe)
            {
                throw fe;
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void ExecuteSafely(Action actionItems)
        {
            try
            {
                actionItems.Invoke();
            }
            catch (FaultException fe)
            {
                throw fe;
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }
    }
}
