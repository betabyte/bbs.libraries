﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.DB
{
  public class CachedResults<T> where T : class, new()
  {
    private static T _cachedResults = new T();
    private static DateTime _expiresDateTime;
    private int _minutesTillExpire;

    public bool IsExpired { get { return _cachedResults == null || DateTime.Now > _expiresDateTime; } }

    public void Cache(T @object)
    {
      _cachedResults = @object;
      _expiresDateTime = DateTime.Now.AddMinutes(_minutesTillExpire);
    }

    public T GetResults()
    {
      return _cachedResults;
    }

    public CachedResults()
    {
      _minutesTillExpire = 30;
    }

    public CachedResults(int minutesTillExpire)
    {
      _minutesTillExpire = minutesTillExpire;
    }
  }
}
