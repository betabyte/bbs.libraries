﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.DB
{
  public class Parameter
  {
    public object Object { get; set; }
    public ParameterType Type { get; set; }

    public string Id { get { return (string.Format("@{0}", this._id.ToString("N"))); } }

    private Guid _id { get; set; }

    public Parameter(object @object, ParameterType parameterType)
    {
      this._id = Guid.NewGuid();
      this.Object = @object;
      this.Type = parameterType;
    }
  }
}
