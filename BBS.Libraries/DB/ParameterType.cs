﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;

namespace BBS.Libraries.DB
{
  public enum ParameterType
  {
    Numeric,
    NumericArray,
    NumericNullable,
    String,
    StringArray,
    Guid,
    GuidNullable,
    GuidArray,
    Bit,
    Blob
  }
}
