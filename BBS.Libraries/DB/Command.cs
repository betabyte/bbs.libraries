﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;

namespace BBS.Libraries.DB
{
  public abstract class Command : BBS.Libraries.Contracts.ICommand
  {
      protected string SqlErrorString { get { return string.Format("Error processing SQL: \n{0}", this.ToString()); } }

      protected string _connectionString { get; set; }

    protected StringBuilder _Command { get; set; }
    private static readonly Regex TrailingCommaRegex = new Regex("(?<FullOperator>\\s*(?<Operator>,)(?<LineSuffix>\\s*))$", RegexOptions.IgnoreCase | RegexOptions.ECMAScript);
    private static readonly Regex TrailingWhereAndOrRegex = new Regex("(?<FullOperator>(?<LinePrefix>\\s+)(?<Operator>where|and|or)(?<LineSuffix>\\s*))$", RegexOptions.IgnoreCase | RegexOptions.ECMAScript);
    private static readonly Regex TrailingUnionRegex = new Regex("(?<FullOperator>\\s+(?<Operator>(union)|(union\\sall))(?<LineSuffix>\\s*))$", RegexOptions.IgnoreCase | RegexOptions.ECMAScript);

    public abstract BBS.Libraries.Contracts.IDataReader Reader { get; set; }

    public void Clear(bool closeReader)
    {
      if (this.Reader != null)
      {
        this._parameters.Clear();
        if (closeReader && !this.Reader.IsClosed)
        {
          this.Reader.Close();
          //this.Reader = null; 
        }
      }
      _Command.Clear();
    }
    
    public void Clear()
    {
      this.Clear(false);
    }

    public void Append(string value)
    {
      _Command.Append(value);
    }

    public void AppendFormat(string value, params object[] values)
    {
      _Command.AppendFormat(value, values);
    }

    public void AppendParameterValue(string value, Parameter parameter)
    {
      if (parameter.Type == ParameterType.GuidArray || parameter.Type == ParameterType.StringArray)
      {
        if (parameter.Object is IEnumerable<Guid>)
        {
          var items = parameter.Object as IEnumerable<Guid>;
          
          value = value.Replace("= {0}", "in ({0})");
          
          _Command.Append(value);
          
          foreach (var item in items)
          {
            var itemParameter = new Parameter(item, parameter.Type);

            var dbParameter = this.CreateParameter() as SqlParameter;
            dbParameter.ParameterName = itemParameter.Id.ToString();
            dbParameter.Value = itemParameter.Object;
            dbParameter.SqlDbType = itemParameter.Type.ToParameterType();

            this._parameters.Add(dbParameter, itemParameter);

            _Command.Replace("{0}", string.Format("{0}, {1}", itemParameter.Id.ToString(), "{0}"));
          }
          _Command.Replace(", {0}", string.Empty);
          this.EndOfList();
        }
      }
      else if (parameter.Type == ParameterType.Bit)
      {
        var dbParameter = this.CreateParameter() as SqlParameter;

        if ((bool)parameter.Object)
        {
          dbParameter.Value = true; 
        }

        if (!(bool)parameter.Object)
        {
          dbParameter.Value = false;
        }
        dbParameter.ParameterName = parameter.Id.ToString();
        dbParameter.SqlDbType = parameter.Type.ToParameterType();
        this._parameters.Add(dbParameter, parameter);

        _Command.AppendFormat(value, parameter.Id.ToString());
      }
      else
      {
        var dbParameter = this.CreateParameter() as SqlParameter;
        dbParameter.ParameterName = parameter.Id.ToString();
        dbParameter.Value = parameter.Object;
        dbParameter.SqlDbType = parameter.Type.ToParameterType();

        this._parameters.Add(dbParameter, parameter);

        _Command.AppendFormat(value, parameter.Id.ToString());
      }

      
    }

    public bool EndOfWhere()
    {
      return EndOfWhere(string.Empty);
    }

    public bool EndOfWhere(string replaceWith)
    {
      bool flag = false;
      string input = ((object)this._Command).ToString();
      Match match = TrailingWhereAndOrRegex.Match(input);
      if (match.Success)
      {
        this._Command.Clear();
        _Command.Append(input.Remove(input.Length - match.Groups["FullOperator"].Length, match.Groups["FullOperator"].Length));
        if (match.Groups["Operator"].Value.Equals("where", StringComparison.InvariantCultureIgnoreCase))
          _Command.Append(match.Groups["LinePrefix"].Value);
        _Command.Append(replaceWith);
        _Command.Append(match.Groups["LineSuffix"].Value);
        flag = true;
      }
      return flag;
    }

    public bool EndOfList()
    {
      return EndOfList(string.Empty);
    }

    public bool EndOfList(string replaceWith)
    {
      bool flag = false;
      string input = ((object)this._Command).ToString();
      Match match = TrailingCommaRegex.Match(input);
      if (match.Success)
      {
        this._Command.Clear();
        _Command.Append(input.Remove(input.Length - match.Groups["FullOperator"].Length, match.Groups["FullOperator"].Length));
        _Command.Append(replaceWith);
        _Command.Append(match.Groups["LineSuffix"].Value);
        flag = true;
      }
      return flag;
    }
    
    public Command(string connectionString)
    {
      _Command = new StringBuilder();

      _connectionString = connectionString;
    }

    public abstract void Open();

    // Testing purposes only
    public Command(string connectionString, IDbConnection connection)
    {
      Connection = connection;
      Connection.ConnectionString = connectionString;
      Connection.Open();
      _Command = new StringBuilder();
    }

    public override string ToString()
    {
      var result = new StringBuilder();

      result.Append(this._Command.ToString());

      foreach (var item in _parameters)
      {
        if (item.Value.Type == ParameterType.Numeric || item.Value.Type == ParameterType.NumericArray || item.Value.Type == ParameterType.NumericNullable)
        {
          result.Replace(item.Key.ParameterName, string.Format("{0}", item.Value.Object.ToString()));
        }
        else if (item.Value.Type == ParameterType.Bit)
        {
          var obj = (bool) item.Value.Object ? 1 : 0;
          result.Replace(item.Key.ParameterName, string.Format("{0}", obj));
        }
        else
        {
          result.Replace(item.Key.ParameterName, string.Format("'{0}'", item.Value.Object.ToString()));
        }
      }

      return result.ToString();
    }

    public void Dispose()
    {
      _Command.Clear();
      Connection.Close();
      Connection.Dispose();
      //Reader.Close();
    }

    public void Prepare()
    {
      throw new NotImplementedException();
    }

    public void Cancel()
    {
      throw new NotImplementedException();
    }

    public IDbDataParameter CreateParameter()
    {
      return new SqlParameter();
    }

    public int ExecuteNonQuery()
    {
      throw new NotImplementedException();
    }

    public System.Data.IDataReader ExecuteReader()
    {
      throw new NotImplementedException();
    }

    public System.Data.IDataReader ExecuteReader(CommandBehavior behavior)
    {
      throw new NotImplementedException();
    }

    public object ExecuteScalar()
    {
      throw new NotImplementedException();
    }

    private IDbConnection _connection;

    protected Command()
    {
      _Command = new StringBuilder();
      _parameters = new Dictionary<SqlParameter, Parameter>();
    }

    protected Dictionary<SqlParameter, Parameter> _parameters;

    public IDbConnection Connection { get; set; }

    public IDbTransaction Transaction { get; set; }
    public string CommandText { get; set; }
    public int CommandTimeout { get; set; }
    public CommandType CommandType { get; set; }
    public IDataParameterCollection Parameters
    {
      get
      {
        throw new NotImplementedException("Not implemented");
      }
    }
    public UpdateRowSource UpdatedRowSource { get; set; }
  }
}
