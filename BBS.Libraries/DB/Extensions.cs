﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;

namespace BBS.Libraries.DB
{
  public static class Extensions
  {
    public static SqlDbType ToParameterType(this ParameterType DbType)
    {
      switch (DbType)
      {
        case ParameterType.Numeric:
          return SqlDbType.Float;
        case ParameterType.Guid:
        case ParameterType.GuidArray:
        case ParameterType.GuidNullable:
          return SqlDbType.UniqueIdentifier;
        case ParameterType.Bit:
          return SqlDbType.Bit;
        case ParameterType.String:
        case ParameterType.StringArray:
        default:
          return SqlDbType.VarChar;
      }
    }
  }
}
