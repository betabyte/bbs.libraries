﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries
{
  public partial class Enums<T>
  {
    private static bool Active(T value)
    {
      FieldInfo fi = value.GetType().GetField(value.ToString());
      var attributes = (Enums.Info[])fi.GetCustomAttributes(typeof(Enums.Info), false);
      if (attributes != null && attributes.Length > 0)
        return attributes[0].Active;
      else
        return false;
    }

    public static IEnumerable<T> ToList(bool active)
    {
      var result = new List<T>();

      var enumValueArray = ValidateEnum();

      foreach (var value in enumValueArray)
      {
        if (active && Active((T) Enum.Parse(typeof (T), value.ToString())))
        {
          result.Add((T) Enum.Parse(typeof (T), value.ToString()));
        }
        else if(!active)
        {
          result.Add((T)Enum.Parse(typeof(T), value.ToString()));
        }
      }

      return result;
    }

    public static IEnumerable<T> ToList()
    {
      return ToList(false);
    }
  }
}
