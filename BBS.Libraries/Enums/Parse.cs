﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries
{
  public partial class Enums<T>
  {
    private static Array ValidateEnum()
    {
      var enumType = typeof (T);
      if (enumType.BaseType != typeof(Enum))
        throw new ArgumentException("T must be type System.Enum");

      return enumType.GetEnumValues();
    }

    
    public static T ParseAbbreviation(string abbreviation)
    {
      var enumValues = ValidateEnum();

      foreach (var e in enumValues)
      {
        if (Abbreviation((Enum)e) == abbreviation)
        {
          return (T)Enum.Parse(typeof(T), e.ToString());
        }
      }

      return default(T);
    }
    public static T ParseCode(string code)
    {
      var enumValues = ValidateEnum();

      foreach (var e in enumValues)
      {
        if (Code((Enum)e) == code)
        {
          return (T)Enum.Parse(typeof(T), e.ToString());
        }
      }

      return default(T);
    }
    public static T ParseName(string name)
    {
      var enumValues = ValidateEnum();

      foreach (var e in enumValues)
      {
        if (Name((Enum)e) == name)
        {
          return (T)Enum.Parse(typeof(T), e.ToString());
        }
      }

      return default(T);
    }
    public static T ParseDescription(string description)
    {
      var enumValues = ValidateEnum();

      foreach (var e in enumValues)
      {
        if (Description((Enum)e) == description)
        {
          return (T)Enum.Parse(typeof(T), e.ToString());
        }
      }

      return default(T);
    }
    public static T Parse(string value)
    {
      var enumValues = ValidateEnum();

      foreach (var e in enumValues)
      {
        if (Value((Enum)e).Equals(value, StringComparison.InvariantCultureIgnoreCase))
        {
          return (T)Enum.Parse(typeof(T), e.ToString());
        }
        if (Abbreviation((Enum)e) == value)
        {
          return (T)Enum.Parse(typeof(T), e.ToString());
        }
        if (Description((Enum)e) == value)
        {
          return (T)Enum.Parse(typeof(T), e.ToString());
        }
        if (Code((Enum)e) == value)
        {
          return (T)Enum.Parse(typeof(T), e.ToString());
        }
      }

      // Try getting by int value 

      int enumValue;
      if (int.TryParse(value, out enumValue))
      {
        return (T)Enum.ToObject(typeof(T), enumValue);
        //return (T)Enum.Parse(typeof(T), enumValue);
      }

      return default(T);
    }

    public static IEnumerable<T> ParseCategory(string category)
    {
      var result = new List<T>();

      var enumValues = ValidateEnum();

      foreach (var e in enumValues)
      {
        if (Category((Enum)e) != null && Category((Enum)e).Contains(category))
        {
          result.Add((T)Enum.Parse(typeof(T), e.ToString()));
        }
      }

      return result;
    }
  }
}
