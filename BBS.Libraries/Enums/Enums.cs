﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries
{
  public partial class Enums
  {
    public string Name { get; set; }
    [System.AttributeUsage(System.AttributeTargets.All, AllowMultiple = false)]
    public class Description : System.ComponentModel.DescriptionAttribute
    {
      public Description(string value) : base(value) { }
    }
    [System.AttributeUsage(System.AttributeTargets.All, AllowMultiple = true)]
    public class Category : System.ComponentModel.CategoryAttribute
    {
      public Category(string value) : base(value) { }
    }
    public Enums(string value) { }
    [System.AttributeUsage(System.AttributeTargets.All, AllowMultiple = false)]
    public class Info : System.Attribute
    {
      public string Name { get; set; }
      public string Value { get; set; }
      public string Abbreviation { get; set; }
      public string Code { get; set; }
      public bool Active { get; set; }
      public string[] Groups { get; set; }
      public object[] Other { get; set; }

      public Info()
      {
        Active = true;
      }
    }
  }
}
