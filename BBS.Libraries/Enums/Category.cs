﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries
{
  public partial class Enums<T>
  {
    public static bool IsInCategory(Enum value, string category)
    {
      ValidateEnum();

      if (Category(value).Contains(category))
      {
        return true;
      }
      return false;
    }

    public static List<string> Category(Enum value)
    {
      FieldInfo fi = value.GetType().GetField(value.ToString());
      var attributes = (CategoryAttribute[])fi.GetCustomAttributes(typeof(CategoryAttribute), false);
      var categories = new List<string>();
      if (attributes != null && attributes.Length > 0)
      {
        for (int i = 0; i < attributes.Length; i++)
        {
          categories.Add(attributes[i].Category);
        }
        return categories;
      }
      else
        return null;
    }
  }
}
