﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries
{
  public partial class Enums<T>
  {
    public static string Name(string abbreviation)
    {
      return Name(ParseAbbreviation(abbreviation) as Enum);
    }

    public static string Name(Enum value)
    {
      FieldInfo fi = value.GetType().GetField(value.ToString());
      var attributes = (Enums.Info[])fi.GetCustomAttributes(typeof(Enums.Info), false);
      if (attributes != null && attributes.Length > 0)
        return attributes[0].Name;
      else
        return value.ToString();
    }
  }
}
