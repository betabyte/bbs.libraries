﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries
{
  public partial class Enums<T>
  {
    public static string Abbreviation(Enum value)
    {
      FieldInfo fi = value.GetType().GetField(value.ToString());
      var attributes = (Enums.Info[])fi.GetCustomAttributes(typeof(Enums.Info), false);
      if (attributes != null && attributes.Length > 0)
        return attributes[0].Abbreviation;
      else
        return value.ToString();
    }
  }
}
