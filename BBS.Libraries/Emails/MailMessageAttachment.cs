﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Emails
{
  public class MailMessageAttachment
  {
    [IgnoreDataMember]
    private Attachment _attachment;

    [DataMember(Order = 1)]
    public byte[] Content { get; set; }

    [DataMember(Order = 2)]
    public string ContentType { get; set; }
    
    [DataMember]
    public string Name { get; set; }
    
    [DataMember]
    public Attachment XAttachment
    {
      get
      {
        if (_attachment == null)
          return _attachment = new Attachment(new MemoryStream(Content), Name, ContentType);
        else
          return _attachment;
      }
      set
      {
        if (_attachment == null)
        {
          _attachment = new Attachment(new MemoryStream(Content), Name, ContentType);
        }
      }
    }
  }
}
