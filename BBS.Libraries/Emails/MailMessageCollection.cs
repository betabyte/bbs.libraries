﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Emails
{
  [CollectionDataContract]
  public class MailMessageCollection : List<MailMessage>
  {
  }
}
