﻿using System;
using System.IO;
using System.Net.Mail;
using System.Runtime.Serialization;
using BBS.Libraries.Extensions;

namespace BBS.Libraries.Emails
{
  public class MailMessageAlternateView
  {
    [IgnoreDataMember]
    private AlternateView _alternateView;

    [DataMember(Order = 1)]
    public byte[] Content { get; set; }
    
    [DataMember(Order = 2)]
    public string ContentType { get; set; }

    [DataMember(Order = 3)]
    public AlternateView XAlternateView
    {
      get
      {
        if (_alternateView == null)
          return _alternateView = new AlternateView(new MemoryStream(Content), ContentType);
        else
          return _alternateView;
      }
      set
      {
        if (_alternateView == null)
        {
          var cs = this.Content;
          _alternateView = new AlternateView(new MemoryStream(cs), ContentType);
        }
      }}
    
  }
}