﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Entities
{
  public enum States
  {
    [BBS.Libraries.Enums.Info(Abbreviation = "AL", Active = true, Name = "Alabama")]
    Alabama,
    [BBS.Libraries.Enums.Info(Abbreviation = "AK", Active = true, Name = "Alaska")]
    Alaska,
    [BBS.Libraries.Enums.Info(Abbreviation = "AR", Active = true, Name = "Arizona")]
    Arizona,
    [BBS.Libraries.Enums.Info(Abbreviation = "AK", Active = true, Name = "Arkansas")]
    Arkansas,
    [BBS.Libraries.Enums.Info(Abbreviation = "CA", Active = true, Name = "California")]
    California,
    [BBS.Libraries.Enums.Info(Abbreviation = "CO", Active = true, Name = "Colorado")]
    Colorado,
    [BBS.Libraries.Enums.Info(Abbreviation = "CT", Active = true, Name = "Connecticut")]
    Connecticut,
    [BBS.Libraries.Enums.Info(Abbreviation = "DE", Active = true, Name = "Delaware")]
    Delaware,
    [BBS.Libraries.Enums.Info(Abbreviation = "DC", Active = true, Name = "District Of Columbia")]
    DistrictOfColumbia,
    [BBS.Libraries.Enums.Info(Abbreviation = "FL", Active = true, Name = "Florida")]
    Florida,
    [BBS.Libraries.Enums.Info(Abbreviation = "GA", Active = true, Name = "Georgia")]
    Georgia,
    [BBS.Libraries.Enums.Info(Abbreviation = "HI", Active = true, Name = "Hawaii")]
    Hawaii,
    [BBS.Libraries.Enums.Info(Abbreviation = "ID", Active = true, Name = "Idaho")]
    Idaho,
    [BBS.Libraries.Enums.Info(Abbreviation = "IL", Active = true, Name = "Illinois")]
    Illinois,
    [BBS.Libraries.Enums.Info(Abbreviation = "IN", Active = true, Name = "Indiana")]
    Indiana,
    [BBS.Libraries.Enums.Info(Abbreviation = "IA", Active = true, Name = "Iowa")]
    Iowa,
    [BBS.Libraries.Enums.Info(Abbreviation = "KS", Active = true, Name = "Kansas")]
    Kansas,
    [BBS.Libraries.Enums.Info(Abbreviation = "KY", Active = true, Name = "Kentucky")]
    Kentucky,
    [BBS.Libraries.Enums.Info(Abbreviation = "LA", Active = true, Name = "Louisiana")]
    Louisiana,
    [BBS.Libraries.Enums.Info(Abbreviation = "ME", Active = true, Name = "Maine")]
    Maine,
    [BBS.Libraries.Enums.Info(Abbreviation = "MD", Active = true, Name = "Maryland")]
    Maryland,
    [BBS.Libraries.Enums.Info(Abbreviation = "MA", Active = true, Name = "Massachusetts")]
    Massachusetts,
    [BBS.Libraries.Enums.Info(Abbreviation = "MI", Active = true, Name = "Michigan")]
    Michigan,
    [BBS.Libraries.Enums.Info(Abbreviation = "MN", Active = true, Name = "Minnesota")]
    Minnesota,
    [BBS.Libraries.Enums.Info(Abbreviation = "MS", Active = true, Name = "Mississippi")]
    Mississippi,
    [BBS.Libraries.Enums.Info(Abbreviation = "MO", Active = true, Name = "Missouri")]
    Missouri,
    [BBS.Libraries.Enums.Info(Abbreviation = "MT", Active = true, Name = "Montana")]
    Montana,
    [BBS.Libraries.Enums.Info(Abbreviation = "NE", Active = true, Name = "Nebraska")]
    Nebraska,
    [BBS.Libraries.Enums.Info(Abbreviation = "NV", Active = true, Name = "Nevada")]
    Nevada,
    [BBS.Libraries.Enums.Info(Abbreviation = "NH", Active = true, Name = "New Hampshire")]
    NewHampshire,
    [BBS.Libraries.Enums.Info(Abbreviation = "NJ", Active = true, Name = "New Jersey")]
    NewJersey,
    [BBS.Libraries.Enums.Info(Abbreviation = "NM", Active = true, Name = "New Mexico")]
    NewMexico,
    [BBS.Libraries.Enums.Info(Abbreviation = "NY", Active = true, Name = "New York")]
    NewYork,
    [BBS.Libraries.Enums.Info(Abbreviation = "NC", Active = true, Name = "NorthCarolina")]
    NorthCarolina,
    [BBS.Libraries.Enums.Info(Abbreviation = "ND", Active = true, Name = "NorthDakota")]
    NorthDakota,
    [BBS.Libraries.Enums.Info(Abbreviation = "OH", Active = true, Name = "Ohio")]
    Ohio,
    [BBS.Libraries.Enums.Info(Abbreviation = "OK", Active = true, Name = "Oklahoma")]
    Oklahoma,
    [BBS.Libraries.Enums.Info(Abbreviation = "OR", Active = true, Name = "Oregon")]
    Oregon,
    [BBS.Libraries.Enums.Info(Abbreviation = "PA", Active = true, Name = "Pennsylvania")]
    Pennsylvania,
    [BBS.Libraries.Enums.Info(Abbreviation = "RI", Active = true, Name = "Rhode Island")]
    RhodeIsland,
    [BBS.Libraries.Enums.Info(Abbreviation = "SC", Active = true, Name = "South Carolina")]
    SouthCarolina,
    [BBS.Libraries.Enums.Info(Abbreviation = "SD", Active = true, Name = "South Dakota")]
    SouthDakota,
    [BBS.Libraries.Enums.Info(Abbreviation = "TN", Active = true, Name = "Tennessee")]
    Tennessee,
    [BBS.Libraries.Enums.Info(Abbreviation = "TX", Active = true, Name = "Texas")]
    Texas,
    [BBS.Libraries.Enums.Info(Abbreviation = "UT", Active = true, Name = "Utah")]
    Utah,
    [BBS.Libraries.Enums.Info(Abbreviation = "VT", Active = true, Name = "Vermont")]
    Vermont,
    [BBS.Libraries.Enums.Info(Abbreviation = "VA", Active = true, Name = "Virginia")]
    Virginia,
    [BBS.Libraries.Enums.Info(Abbreviation = "WA", Active = true, Name = "Washington")]
    Washington,
    [BBS.Libraries.Enums.Info(Abbreviation = "WV", Active = true, Name = "West Virginia")]
    WestVirginia,
    [BBS.Libraries.Enums.Info(Abbreviation = "WI", Active = true, Name = "Wisconsin")]
    Wisconsin,
    [BBS.Libraries.Enums.Info(Abbreviation = "WY", Active = true, Name = "Wyoming")]
    Wyoming
  }
}
