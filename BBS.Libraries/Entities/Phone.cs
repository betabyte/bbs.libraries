﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace BBS.Libraries.Entities
{
  [DataContract]
  public partial class Phone
  {
    [DataMember]
    public string AreaCode { get; set; }
    [DataMember]
    public string Exchange { get; set; }
    [DataMember]
    public string Suffix { get; set; }
    [DataMember]
    public string Extension { get; set; }
    [DataMember]
    public bool IsValid { get; set; }

    public Phone()
    {
    }

    public Phone(string number)
    {
      var phoneNumber = Regex.Replace(number, "[^.0-9]", string.Empty);

      var phoneRegex = new Regex(@"(?<areaCode>\d{3})(?<exchange>\d{3})(?<suffix>\d{4})");

      if (phoneRegex.IsMatch(phoneNumber))
      {
        var brokenPhone = phoneRegex.Match(phoneNumber);

        AreaCode = brokenPhone.Groups["areaCode"].Value;
        Exchange = brokenPhone.Groups["exchange"].Value;
        Suffix = brokenPhone.Groups["suffix"].Value;
        IsValid = true;
      }
      else
      {
        IsValid = false;
      }
    }

    public override string ToString()
    {
      if (IsValid)
      {
        return string.Format("({0}) {1}-{2}", AreaCode, Exchange, Suffix);
      }
      return string.Empty;
    }
  }
}
