﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Entities
{
  public enum Countries
  {
    [BBS.Libraries.Enums.Info(Abbreviation = "AF", Active = true, Name = "Afghanistan")] Afghanistan,
    [BBS.Libraries.Enums.Info(Abbreviation = "AL", Active = true, Name = "Albania")] Albania,
    [BBS.Libraries.Enums.Info(Abbreviation = "DZ", Active = true, Name = "Algeria")] Algeria, 
    [BBS.Libraries.Enums.Info(Abbreviation = "AS", Active = true, Name = "American Samoa")] AmericanSamoa,
    [BBS.Libraries.Enums.Info(Abbreviation = "AD", Active = true, Name = "Andorra")] Andorra,
    [BBS.Libraries.Enums.Info(Abbreviation = "AO", Active = true, Name = "Angola")] Angola,
    [BBS.Libraries.Enums.Info(Abbreviation = "AI", Active = true, Name = "Anguilla")] Anguilla,
    [BBS.Libraries.Enums.Info(Abbreviation = "AG", Active = true, Name = "Antigua and Barbuda")] AntiguaAndBarbuda,
    [BBS.Libraries.Enums.Info(Abbreviation = "AR", Active = true, Name = "Argentina")] Argentina,
    [BBS.Libraries.Enums.Info(Abbreviation = "AM", Active = true, Name = "Armenia")] Armenia,
    [BBS.Libraries.Enums.Info(Abbreviation = "AW", Active = true, Name = "Aruba")] Aruba,
    [BBS.Libraries.Enums.Info(Abbreviation = "AU", Active = true, Name = "Australia")] Australia,
    [BBS.Libraries.Enums.Info(Abbreviation = "AT", Active = true, Name = "Austria")] Austria,
    [BBS.Libraries.Enums.Info(Abbreviation = "AZ", Active = true, Name = "Azerbaijan")] Azerbaijan,
    [BBS.Libraries.Enums.Info(Abbreviation = "A2", Active = true, Name = "Azores")] Azores,
    [BBS.Libraries.Enums.Info(Abbreviation = "BS", Active = true, Name = "Bahamas")] Bahamas,
    [BBS.Libraries.Enums.Info(Abbreviation = "BH", Active = true, Name = "Bahrain")] Bahrain,
    [BBS.Libraries.Enums.Info(Abbreviation = "BD", Active = true, Name = "Bangladesh")] Bangladesh,
    [BBS.Libraries.Enums.Info(Abbreviation = "BB", Active = true, Name = "Barbados")] Barbados,
    [BBS.Libraries.Enums.Info(Abbreviation = "BY", Active = true, Name = "Belarus")] Belarus,
    [BBS.Libraries.Enums.Info(Abbreviation = "BE", Active = true, Name = "Belgium")] Belgium,
    [BBS.Libraries.Enums.Info(Abbreviation = "BZ", Active = true, Name = "Belize")] Belize,
    [BBS.Libraries.Enums.Info(Abbreviation = "BJ", Active = true, Name = "Benin")] Benin,
    [BBS.Libraries.Enums.Info(Abbreviation = "BM", Active = true, Name = "Bermuda")] Bermuda,
    [BBS.Libraries.Enums.Info(Abbreviation = "BT", Active = true, Name = "Bhutan")] Bhutan,
    [BBS.Libraries.Enums.Info(Abbreviation = "BO", Active = true, Name = "Bolivia")] Bolivia,
    [BBS.Libraries.Enums.Info(Abbreviation = "BL", Active = true, Name = "Bonaire")] Bonaire,
    [BBS.Libraries.Enums.Info(Abbreviation = "BA", Active = true, Name = "Bosnia")] Bosnia,
    [BBS.Libraries.Enums.Info(Abbreviation = "BW", Active = true, Name = "Botswana")] Botswana,
    [BBS.Libraries.Enums.Info(Abbreviation = "BR", Active = true, Name = "Brazil")] Brazil,
    [BBS.Libraries.Enums.Info(Abbreviation = "VG", Active = true, Name = "British Virgin Islands")] BritishVirginIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "BN", Active = true, Name = "Brunei")] Brunei,
    [BBS.Libraries.Enums.Info(Abbreviation = "BG", Active = true, Name = "Bulgaria")] Bulgaria,
    [BBS.Libraries.Enums.Info(Abbreviation = "BF", Active = true, Name = "Burkina Faso")] BurkinaFaso,
    [BBS.Libraries.Enums.Info(Abbreviation = "BI", Active = true, Name = "Burundi")] Burundi,
    [BBS.Libraries.Enums.Info(Abbreviation = "KH", Active = true, Name = "Cambodia")] Cambodia,
    [BBS.Libraries.Enums.Info(Abbreviation = "CM", Active = true, Name = "Cameroon")] Cameroon,
    [BBS.Libraries.Enums.Info(Abbreviation = "CA", Active = true, Name = "Canada")] Canada,
    [BBS.Libraries.Enums.Info(Abbreviation = "IC", Active = true, Name = "Canary Islands")] CanaryIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "CV", Active = true, Name = "Cape Verde Island")] CapeVerdeIsland,
    [BBS.Libraries.Enums.Info(Abbreviation = "KY", Active = true, Name = "Cayman Islands")] CaymanIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "CF", Active = true, Name = "Central African Republic")] CentralAfricanRepublic,
    [BBS.Libraries.Enums.Info(Abbreviation = "TD", Active = true, Name = "Chad")] Chad,
    [BBS.Libraries.Enums.Info(Abbreviation = "CL", Active = true, Name = "Chile")] Chile,
    [BBS.Libraries.Enums.Info(Abbreviation = "CN", Active = true, Name = "China, People’s Republic of")] China,
    [BBS.Libraries.Enums.Info(Abbreviation = "CO", Active = true, Name = "Colombia")] Colombia,
    [BBS.Libraries.Enums.Info(Abbreviation = "KM", Active = true, Name = "Comoros")] Comoros,
    [BBS.Libraries.Enums.Info(Abbreviation = "CG", Active = true, Name = "Congo")] Congo,
    [BBS.Libraries.Enums.Info(Abbreviation = "CD", Active = true, Name = "Congo, The Democratic Republic of")] CongoRepublic,
    [BBS.Libraries.Enums.Info(Abbreviation = "CK", Active = true, Name = "Cook Islands")] CookIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "CR", Active = true, Name = "Costa Rica")] CostaRica,
    [BBS.Libraries.Enums.Info(Abbreviation = "HR", Active = true, Name = "Croatia")] Croatia,
    [BBS.Libraries.Enums.Info(Abbreviation = "CB", Active = true, Name = "Curacao")] Curacao,
    [BBS.Libraries.Enums.Info(Abbreviation = "CY", Active = true, Name = "Cyprus")] Cyprus,
    [BBS.Libraries.Enums.Info(Abbreviation = "CZ", Active = true, Name = "Czech Republic")] CzechRepublic,
    [BBS.Libraries.Enums.Info(Abbreviation = "DK", Active = true, Name = "Denmark")] Denmark,
    [BBS.Libraries.Enums.Info(Abbreviation = "DJ", Active = true, Name = "Djibouti")] Djibouti,
    [BBS.Libraries.Enums.Info(Abbreviation = "DM", Active = true, Name = "Dominica")] Dominica,
    [BBS.Libraries.Enums.Info(Abbreviation = "DO", Active = true, Name = "Dominican Republic")] DominicanRepublic,
    [BBS.Libraries.Enums.Info(Abbreviation = "TL", Active = true, Name = "East Timor")] EastTimor,
    [BBS.Libraries.Enums.Info(Abbreviation = "EC", Active = true, Name = "Ecuador")] Ecuador,
    [BBS.Libraries.Enums.Info(Abbreviation = "EG", Active = true, Name = "Egypt")] Egypt,
    [BBS.Libraries.Enums.Info(Abbreviation = "SV", Active = true, Name = "El Salvador")] ElSalvador,
    [BBS.Libraries.Enums.Info(Abbreviation = "EN", Active = true, Name = "England")] England,
    [BBS.Libraries.Enums.Info(Abbreviation = "GQ", Active = true, Name = "Equatorial Guinea")] EquatorialGuinea,
    [BBS.Libraries.Enums.Info(Abbreviation = "ER", Active = true, Name = "Eritrea")] Eritrea,
    [BBS.Libraries.Enums.Info(Abbreviation = "EE", Active = true, Name = "Estonia")] Estonia,
    [BBS.Libraries.Enums.Info(Abbreviation = "ET", Active = true, Name = "Ethiopia")] Ethiopia,
    [BBS.Libraries.Enums.Info(Abbreviation = "EP", Active = true, Name = "Europe")] Europe,
    [BBS.Libraries.Enums.Info(Abbreviation = "FO", Active = true, Name = "Faeroe Islands")] FaeroeIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "FJ", Active = true, Name = "Fiji")] Fiji,
    [BBS.Libraries.Enums.Info(Abbreviation = "FI", Active = true, Name = "Finland")] Finland,
    [BBS.Libraries.Enums.Info(Abbreviation = "FR", Active = true, Name = "France")] France,
    [BBS.Libraries.Enums.Info(Abbreviation = "GF", Active = true, Name = "French Guiana")] FrenchGuiana,
    [BBS.Libraries.Enums.Info(Abbreviation = "PF", Active = true, Name = "French Polynesia")] FrenchPolynesia,
    [BBS.Libraries.Enums.Info(Abbreviation = "GA", Active = true, Name = "Gabon")] Gabon,
    [BBS.Libraries.Enums.Info(Abbreviation = "GM", Active = true, Name = "Gambia")] Gambia,
    [BBS.Libraries.Enums.Info(Abbreviation = "GE", Active = true, Name = "Georgia")] Georgia,
    [BBS.Libraries.Enums.Info(Abbreviation = "DE", Active = true, Name = "Germany")] Germany,
    [BBS.Libraries.Enums.Info(Abbreviation = "GH", Active = true, Name = "Ghana")] Ghana,
    [BBS.Libraries.Enums.Info(Abbreviation = "GI", Active = true, Name = "Gibraltar")]  Gibraltar,
    [BBS.Libraries.Enums.Info(Abbreviation = "GR", Active = true, Name = "Greece")] Greece,
    [BBS.Libraries.Enums.Info(Abbreviation = "GL", Active = true, Name = "Greenland")] Greenland,
    [BBS.Libraries.Enums.Info(Abbreviation = "GD", Active = true, Name = "Grenada")] Grenada,
    [BBS.Libraries.Enums.Info(Abbreviation = "GP", Active = true, Name = "Guadeloupe")] Guadeloupe,
    [BBS.Libraries.Enums.Info(Abbreviation = "GU", Active = true, Name = "Guam")] Guam,
    [BBS.Libraries.Enums.Info(Abbreviation = "GT", Active = true, Name = "Guatemala")] Guatemala,
    [BBS.Libraries.Enums.Info(Abbreviation = "GG", Active = true, Name = "Guernsey")] Guernsey,
    [BBS.Libraries.Enums.Info(Abbreviation = "GN", Active = true, Name = "Guinea")] Guinea,
    [BBS.Libraries.Enums.Info(Abbreviation = "GW", Active = true, Name = "Guinea-Bissau")] GuineaBissau,
    [BBS.Libraries.Enums.Info(Abbreviation = "GY", Active = true, Name = "Guyana")] Guyana,
    [BBS.Libraries.Enums.Info(Abbreviation = "HT", Active = true, Name = "Haiti")] Haiti,
    [BBS.Libraries.Enums.Info(Abbreviation = "HO", Active = true, Name = "Holland")] Holland,
    [BBS.Libraries.Enums.Info(Abbreviation = "HN", Active = true, Name = "Honduras")] Honduras,
    [BBS.Libraries.Enums.Info(Abbreviation = "HK", Active = true, Name = "Hong Kong")] HongKong,
    [BBS.Libraries.Enums.Info(Abbreviation = "HU", Active = true, Name = "Hungary")] Hungary,
    [BBS.Libraries.Enums.Info(Abbreviation = "IS", Active = true, Name = "Iceland")] Iceland,
    [BBS.Libraries.Enums.Info(Abbreviation = "IN", Active = true, Name = "India")] India,
    [BBS.Libraries.Enums.Info(Abbreviation = "ID", Active = true, Name = "Indonesia")] Indonesia,
    [BBS.Libraries.Enums.Info(Abbreviation = "IQ", Active = true, Name = "Iraq")] Iraq,
    [BBS.Libraries.Enums.Info(Abbreviation = "IE", Active = true, Name = "Ireland, Republic of")] Ireland,
    [BBS.Libraries.Enums.Info(Abbreviation = "IL", Active = true, Name = "Israel")] Israel,
    [BBS.Libraries.Enums.Info(Abbreviation = "IT", Active = true, Name = "Italy")] Italy,
    [BBS.Libraries.Enums.Info(Abbreviation = "CI", Active = true, Name = "Ivory Coast")] IvoryCoast,
    [BBS.Libraries.Enums.Info(Abbreviation = "JM", Active = true, Name = "Jamaica")] Jamaica,
    [BBS.Libraries.Enums.Info(Abbreviation = "JP", Active = true, Name = "Japan")] Japan,
    [BBS.Libraries.Enums.Info(Abbreviation = "JE", Active = true, Name = "Jersey")] Jersey,
    [BBS.Libraries.Enums.Info(Abbreviation = "JO", Active = true, Name = "Jordan")] Jordan,
    [BBS.Libraries.Enums.Info(Abbreviation = "KZ", Active = true, Name = "Kazakhstan")] Kazakhstan,
    [BBS.Libraries.Enums.Info(Abbreviation = "KE", Active = true, Name = "Kenya")] Kenya,
    [BBS.Libraries.Enums.Info(Abbreviation = "KI", Active = true, Name = "Kiribati")] Kiribati,
    [BBS.Libraries.Enums.Info(Abbreviation = "KR", Active = true, Name = "Korea, South")] Korea,
    [BBS.Libraries.Enums.Info(Abbreviation = "KO", Active = true, Name = "Kosrae")] Kosrae,
    [BBS.Libraries.Enums.Info(Abbreviation = "KW", Active = true, Name = "Kuwait")] Kuwait,
    [BBS.Libraries.Enums.Info(Abbreviation = "KG", Active = true, Name = "Kyrgyzstan")] Kyrgyzstan,
    [BBS.Libraries.Enums.Info(Abbreviation = "LA", Active = true, Name = "Laos")] Laos,
    [BBS.Libraries.Enums.Info(Abbreviation = "LV", Active = true, Name = "Latvia")] Latvia,
    [BBS.Libraries.Enums.Info(Abbreviation = "LB", Active = true, Name = "Lebanon")] Lebanon,
    [BBS.Libraries.Enums.Info(Abbreviation = "LS", Active = true, Name = "Lesotho")] Lesotho,
    [BBS.Libraries.Enums.Info(Abbreviation = "LR", Active = true, Name = "Liberia")] Liberia,
    [BBS.Libraries.Enums.Info(Abbreviation = "LY", Active = true, Name = "Libya")] Libya,
    [BBS.Libraries.Enums.Info(Abbreviation = "LI", Active = true, Name = "Liechtenstein")]
    Liechtenstein,
    [BBS.Libraries.Enums.Info(Abbreviation = "LT", Active = true, Name = "Lithuania")]
    Lithuania,
    [BBS.Libraries.Enums.Info(Abbreviation = "LU", Active = true, Name = "Luxembourg")]
    Luxembourg,
    [BBS.Libraries.Enums.Info(Abbreviation = "MO", Active = true, Name = "Macau")]
    Macau,
    [BBS.Libraries.Enums.Info(Abbreviation = "MK", Active = true, Name = "Macedonia (FYROM")]
    MacedoniaFYROM,
    [BBS.Libraries.Enums.Info(Abbreviation = "MG", Active = true, Name = "Madagascar")]
    Madagascar,
    [BBS.Libraries.Enums.Info(Abbreviation = "M3", Active = true, Name = "Madeira")]
    Madeira,
    [BBS.Libraries.Enums.Info(Abbreviation = "MW", Active = true, Name = "Malawi")]
    Malawi,
    [BBS.Libraries.Enums.Info(Abbreviation = "MY", Active = true, Name = "Malaysia")]
    Malaysia,
    [BBS.Libraries.Enums.Info(Abbreviation = "MV", Active = true, Name = "Maldives")]
    Maldives,
    [BBS.Libraries.Enums.Info(Abbreviation = "ML", Active = true, Name = "Mali")]
    Mali,
    [BBS.Libraries.Enums.Info(Abbreviation = "MT", Active = true, Name = "Malta")]
    Malta,
    [BBS.Libraries.Enums.Info(Abbreviation = "MH", Active = true, Name = "Marshall Islands")]
    MarshallIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "MQ", Active = true, Name = "Martinique")]
    Martinique,
    [BBS.Libraries.Enums.Info(Abbreviation = "MR", Active = true, Name = "Mauritania")]
    Mauritania,
    [BBS.Libraries.Enums.Info(Abbreviation = "MU", Active = true, Name = "Mauritius")]
    Mauritius,
    [BBS.Libraries.Enums.Info(Abbreviation = "YT", Active = true, Name = "Mayotte")]
    Mayotte,
    [BBS.Libraries.Enums.Info(Abbreviation = "MX", Active = true, Name = "Mexico")]
    Mexico,
    [BBS.Libraries.Enums.Info(Abbreviation = "FM", Active = true, Name = "Micronesia, Federated States of")]
    Micronesia,
    [BBS.Libraries.Enums.Info(Abbreviation = "MD", Active = true, Name = "Moldova")]
    Moldova,
    [BBS.Libraries.Enums.Info(Abbreviation = "MC", Active = true, Name = "Monaco")]
    Monaco,
    [BBS.Libraries.Enums.Info(Abbreviation = "MN", Active = true, Name = "Mongolia")]
    Mongolia,
    [BBS.Libraries.Enums.Info(Abbreviation = "ME", Active = true, Name = "Montenegro")]
    Montenegro,
    [BBS.Libraries.Enums.Info(Abbreviation = "MS", Active = true, Name = "Montserrat")]
    Montserrat,
    [BBS.Libraries.Enums.Info(Abbreviation = "MA", Active = true, Name = "Morocco")]
    Morocco,
    [BBS.Libraries.Enums.Info(Abbreviation = "MZ", Active = true, Name = "Mozambique")]
    Mozambique,
    [BBS.Libraries.Enums.Info(Abbreviation = "NA", Active = true, Name = "Namibia")]
    Namibia,
    [BBS.Libraries.Enums.Info(Abbreviation = "NP", Active = true, Name = "Nepal")]
    Nepal,
    [BBS.Libraries.Enums.Info(Abbreviation = "NL", Active = true, Name = "Netherlands")]
    Netherlands,
    [BBS.Libraries.Enums.Info(Abbreviation = "AN", Active = true, Name = "Netherlands Antilles")]
    NetherlandsAntilles,
    [BBS.Libraries.Enums.Info(Abbreviation = "NC", Active = true, Name = "New Caledonia")]
    NewCaledonia,
    [BBS.Libraries.Enums.Info(Abbreviation = "NZ", Active = true, Name = "New Zealand")]
    NewZealand,
    [BBS.Libraries.Enums.Info(Abbreviation = "NI", Active = true, Name = "Nicaragua")]
    Nicaragua,
    [BBS.Libraries.Enums.Info(Abbreviation = "NE", Active = true, Name = "Niger")]
    Niger,
    [BBS.Libraries.Enums.Info(Abbreviation = "NG", Active = true, Name = "Nigeria")]
    Nigeria,
    [BBS.Libraries.Enums.Info(Abbreviation = "NF", Active = true, Name = "Norfolk Island")]
    NorfolkIsland,
    [BBS.Libraries.Enums.Info(Abbreviation = "NB", Active = true, Name = "Northern Ireland")]
    NorthernIreland,
    [BBS.Libraries.Enums.Info(Abbreviation = "MP", Active = true, Name = "Northern Mariana Islands")]
    NorthernMarianaIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "NO", Active = true, Name = "Norway")]
    Norway,
    [BBS.Libraries.Enums.Info(Abbreviation = "OM", Active = true, Name = "Oman")]
    Oman,
    [BBS.Libraries.Enums.Info(Abbreviation = "PK", Active = true, Name = "Pakistan")]
    Pakistan,
    [BBS.Libraries.Enums.Info(Abbreviation = "PW", Active = true, Name = "Palau")]
    Palau,
    [BBS.Libraries.Enums.Info(Abbreviation = "PA", Active = true, Name = "Panama")]
    Panama,
    [BBS.Libraries.Enums.Info(Abbreviation = "PG", Active = true, Name = "Papua New Guinea")]
    PapuaNewGuinea,
    [BBS.Libraries.Enums.Info(Abbreviation = "PY", Active = true, Name = "Paraguay")]
    Paraguay,
    [BBS.Libraries.Enums.Info(Abbreviation = "PE", Active = true, Name = "Peru")]
    Peru,
    [BBS.Libraries.Enums.Info(Abbreviation = "PH", Active = true, Name = "Philippines")]
    Philippines,
    [BBS.Libraries.Enums.Info(Abbreviation = "PL", Active = true, Name = "Poland")]
    Poland,
    [BBS.Libraries.Enums.Info(Abbreviation = "PO", Active = true, Name = "Ponape")]
    Ponape,
    [BBS.Libraries.Enums.Info(Abbreviation = "PT", Active = true, Name = "Portugal")]
    Portugal,
    [BBS.Libraries.Enums.Info(Abbreviation = "PR", Active = true, Name = "Puerto Rico")]
    PuertoRico,
    [BBS.Libraries.Enums.Info(Abbreviation = "QA", Active = true, Name = "Qatar")]
    Qatar,
    [BBS.Libraries.Enums.Info(Abbreviation = "RE", Active = true, Name = "Reunion")]
    Reunion,
    [BBS.Libraries.Enums.Info(Abbreviation = "RO", Active = true, Name = "Romania")]
    Romania,
    [BBS.Libraries.Enums.Info(Abbreviation = "RT", Active = true, Name = "Rota")]
    Rota,
    [BBS.Libraries.Enums.Info(Abbreviation = "RU", Active = true, Name = "Russia")]
    Russia,
    [BBS.Libraries.Enums.Info(Abbreviation = "RW", Active = true, Name = "Rwanda")]
    Rwanda,
    [BBS.Libraries.Enums.Info(Abbreviation = "SS", Active = true, Name = "Saba")]
    Saba,
    [BBS.Libraries.Enums.Info(Abbreviation = "SP", Active = true, Name = "Saipan")]
    Saipan,
    [BBS.Libraries.Enums.Info(Abbreviation = "WS", Active = true, Name = "Samoa")]
    Samoa,
    [BBS.Libraries.Enums.Info(Abbreviation = "SM", Active = true, Name = "San Marino")]
    SanMarino,
    [BBS.Libraries.Enums.Info(Abbreviation = "SA", Active = true, Name = "Saudi Arabia")]
    SaudiArabia,
    [BBS.Libraries.Enums.Info(Abbreviation = "SF", Active = true, Name = "Scotland")]
    Scotland,
    [BBS.Libraries.Enums.Info(Abbreviation = "SN", Active = true, Name = "Senegal")]
    Senegal,
    [BBS.Libraries.Enums.Info(Abbreviation = "RS", Active = true, Name = "Serbia")]
    Serbia,
    [BBS.Libraries.Enums.Info(Abbreviation = "SC", Active = true, Name = "Seychelles")]
    Seychelles,
    [BBS.Libraries.Enums.Info(Abbreviation = "SL", Active = true, Name = "Sierra Leone")]
    SierraLeone,
    [BBS.Libraries.Enums.Info(Abbreviation = "SG", Active = true, Name = "Singapore")]
    Singapore,
    [BBS.Libraries.Enums.Info(Abbreviation = "SK", Active = true, Name = "Slovakia")]
    Slovakia,
    [BBS.Libraries.Enums.Info(Abbreviation = "SI", Active = true, Name = "Slovenia")]
    Slovenia,
    [BBS.Libraries.Enums.Info(Abbreviation = "SB", Active = true, Name = "Solomon Islands")]
    SolomonIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "ZA", Active = true, Name = "South Africa")]
    SouthAfrica,
    [BBS.Libraries.Enums.Info(Abbreviation = "ES", Active = true, Name = "Spain")]
    Spain,
    [BBS.Libraries.Enums.Info(Abbreviation = "LK", Active = true, Name = "Sri Lanka")]
    SriLanka,
    [BBS.Libraries.Enums.Info(Abbreviation = "NT", Active = true, Name = "St. Barthelemy")]
    StBarthelemy,
    [BBS.Libraries.Enums.Info(Abbreviation = "SW", Active = true, Name = "St. Christopher")]
    StChristopher,
    [BBS.Libraries.Enums.Info(Abbreviation = "SX", Active = true, Name = "St. Croix")]
    StCroix,
    [BBS.Libraries.Enums.Info(Abbreviation = "EU", Active = true, Name = "St. Eustatius")]
    StEustatius,
    [BBS.Libraries.Enums.Info(Abbreviation = "UV", Active = true, Name = "St. John")]
    StJohn,
    [BBS.Libraries.Enums.Info(Abbreviation = "KN", Active = true, Name = "St. Kitts and Nevis")]
    StKittsAndNevis,
    [BBS.Libraries.Enums.Info(Abbreviation = "LC", Active = true, Name = "St. Lucia")]
    StLucia,
    [BBS.Libraries.Enums.Info(Abbreviation = "MB", Active = true, Name = "St. Maarten")]
    StMaarten,
    [BBS.Libraries.Enums.Info(Abbreviation = "TB", Active = true, Name = "St. Martin")]
    StMartin,
    [BBS.Libraries.Enums.Info(Abbreviation = "VL", Active = true, Name = "St. Thomas")]
    StThomas,
    [BBS.Libraries.Enums.Info(Abbreviation = "VC", Active = true, Name = "St. Vincent and the Grenadines")]
    StVincentAndTheGrenadines,
    [BBS.Libraries.Enums.Info(Abbreviation = "SR", Active = true, Name = "Suriname")]
    Suriname,
    [BBS.Libraries.Enums.Info(Abbreviation = "SZ", Active = true, Name = "Swaziland")]
    Swaziland,
    [BBS.Libraries.Enums.Info(Abbreviation = "SE", Active = true, Name = "Sweden")]
    Sweden,
    [BBS.Libraries.Enums.Info(Abbreviation = "CH", Active = true, Name = "Switzerland")]
    Switzerland,
    [BBS.Libraries.Enums.Info(Abbreviation = "TA", Active = true, Name = "Tahiti")]
    Tahiti,
    [BBS.Libraries.Enums.Info(Abbreviation = "TW", Active = true, Name = "Taiwan")]
    Taiwan,
    [BBS.Libraries.Enums.Info(Abbreviation = "TJ", Active = true, Name = "Tajikistan")]
    Tajikistan,
    [BBS.Libraries.Enums.Info(Abbreviation = "TZ", Active = true, Name = "Tanzania")]
    Tanzania,
    [BBS.Libraries.Enums.Info(Abbreviation = "TH", Active = true, Name = "Thailand")]
    Thailand,
    [BBS.Libraries.Enums.Info(Abbreviation = "TI", Active = true, Name = "Tinian")]
    Tinian,
    [BBS.Libraries.Enums.Info(Abbreviation = "TG", Active = true, Name = "Togo")]
    Togo,
    [BBS.Libraries.Enums.Info(Abbreviation = "TO", Active = true, Name = "Tonga")]
    Tonga,
    [BBS.Libraries.Enums.Info(Abbreviation = "ZZ", Active = true, Name = "Tortola")]
    Tortola,
    [BBS.Libraries.Enums.Info(Abbreviation = "TT", Active = true, Name = "Trinidad and Tobago")]
    TrinidadAndTobago,
    [BBS.Libraries.Enums.Info(Abbreviation = "TU", Active = true, Name = "Truk")]
    Truk,
    [BBS.Libraries.Enums.Info(Abbreviation = "TN", Active = true, Name = "Tunisia")]
    Tunisia,
    [BBS.Libraries.Enums.Info(Abbreviation = "TR", Active = true, Name = "Turkey")]
    Turkey,
    [BBS.Libraries.Enums.Info(Abbreviation = "TM", Active = true, Name = "Turkmenistan")]
    Turkmenistan,
    [BBS.Libraries.Enums.Info(Abbreviation = "TC", Active = true, Name = "Turks and Caicos Islands")]
    TurksAndCaicosIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "TV", Active = true, Name = "Tuvalu")]
    Tuvalu,
    [BBS.Libraries.Enums.Info(Abbreviation = "VI", Active = true, Name = "U.S. Virgin Islands")]
    USVirginIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "UG", Active = true, Name = "Uganda")]
    Uganda,
    [BBS.Libraries.Enums.Info(Abbreviation = "UA", Active = true, Name = "Ukraine")]
    Ukraine,
    [BBS.Libraries.Enums.Info(Abbreviation = "UI", Active = true, Name = "Union Island")]
    UnionIsland,
    [BBS.Libraries.Enums.Info(Abbreviation = "AE", Active = true, Name = "United Arab Emirates")]
    UnitedArabEmirates,
    [BBS.Libraries.Enums.Info(Abbreviation = "GB", Active = true, Name = "United Kingdom")]
    UnitedKingdom,
    [BBS.Libraries.Enums.Info(Abbreviation = "US", Active = true, Name = "United States")]
    UnitedStates,
    [BBS.Libraries.Enums.Info(Abbreviation = "UY", Active = true, Name = "Uruguay")]
    Uruguay,
    [BBS.Libraries.Enums.Info(Abbreviation = "UZ", Active = true, Name = "Uzbekistan")]
    Uzbekistan,
    [BBS.Libraries.Enums.Info(Abbreviation = "VU", Active = true, Name = "Vanuatu")]
    Vanuatu,
    [BBS.Libraries.Enums.Info(Abbreviation = "VA", Active = true, Name = "Vatican City State")]
    VaticanCityState,
    [BBS.Libraries.Enums.Info(Abbreviation = "VE", Active = true, Name = "Venezuela")]
    Venezuela,
    [BBS.Libraries.Enums.Info(Abbreviation = "VN", Active = true, Name = "Vietnam")]
    Vietnam,
    [BBS.Libraries.Enums.Info(Abbreviation = "VR", Active = true, Name = "Virgin Gorda")]
    VirginGorda,
    [BBS.Libraries.Enums.Info(Abbreviation = "WL", Active = true, Name = "Wales")]
    Wales,
    [BBS.Libraries.Enums.Info(Abbreviation = "WF", Active = true, Name = "Wallis and Futuna Islands")]
    WallisAndFutunaIslands,
    [BBS.Libraries.Enums.Info(Abbreviation = "YA", Active = true, Name = "Yap")]
    Yap,
    [BBS.Libraries.Enums.Info(Abbreviation = "YE", Active = true, Name = "Yemen")]
    Yemen,
    [BBS.Libraries.Enums.Info(Abbreviation = "ZM", Active = true, Name = "Zambia")]
    Zambia,
    [BBS.Libraries.Enums.Info(Abbreviation = "ZW", Active = true, Name = "Zimbabwe")]
    Zimbabwe
  }
}
