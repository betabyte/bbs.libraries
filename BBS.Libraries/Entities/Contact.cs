﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Entities
{
  public abstract class Contact
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public Emails.EmailAddressCollection EmailAddresses { get; set; }
    public Phone Phones { get; set; }

    public string Name { get { return string.Format("{0} {1}", FirstName, LastName); } }
  }
}
