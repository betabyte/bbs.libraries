﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace BBS.Libraries.Log
{
    public class StatusTracker
    {
        private StringBuilder _logger;
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(Assembly.GetEntryAssembly().GetName().ToString());
        public string Log
        {
            get { return _logger.ToString(); }
        }

        public override string ToString()
        {
            return Log;
        }

        public StatusTracker()
        {
            _logger = new StringBuilder();
        }

        public void AddToLog(string value)
        {
            var logString = string.Format("{0:MM/dd/yyyy hh:mm:ss:fff} - {1} \n", DateTime.Now, value);
            
            logger.Info(logString);
            
            _logger.Append(logString);
        }

        public void AddToLog(Exception exception)
        {
            var logString = string.Format("EXCEPTION: {0:MM/dd/yyyy hh:mm:ss:fff} - {1} \n\t{2}", DateTime.Now,
                exception.Message, exception.StackTrace);
            
            logger.Error(logString);
            
            _logger.Append(logString);
        }
    }
}
