﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;

using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Unity;
using Microsoft.Practices.Unity;


namespace BBS.Libraries.Mef
{
    public static partial class Mef
    {
        public static T Get<T>()
        {
            return UnityContainer.Resolve<T>();
        }
    }
}
