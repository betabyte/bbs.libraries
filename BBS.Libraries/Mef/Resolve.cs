﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;

using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Unity;
using Microsoft.Practices.Unity;

namespace BBS.Libraries.Mef
{
    public static partial class Mef
    {
        public static void Resolve(params object[] attributedObjects)
        {
            var assemblyCatalog = new AssemblyCatalog(Assembly.GetEntryAssembly());

            var aggregateCatalog = new AggregateCatalog();
            aggregateCatalog.Catalogs.Add(assemblyCatalog);

            if (Configuration.Current.MefParts != null)
            {
                foreach (var part in Configuration.Current.MefParts)
                {
                    var directoryCatalog = new DirectoryCatalog(part.FileLocation);
                    aggregateCatalog.Catalogs.Add(directoryCatalog);
                }
            }

            UnityContainer = Bootstrapper.BuildUnityContainer(aggregateCatalog);

            var container = UnityContainer.Resolve<CompositionContainer>();
            container.ComposeParts(attributedObjects);
        }

    }
}
