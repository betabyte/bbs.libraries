﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Web.Attributes
{
  [System.AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
  public class RouteNameAttribute : Attribute
  {
    
  }
}
