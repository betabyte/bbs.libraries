﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BBS.Libraries.Entities.Security;

namespace BBS.Libraries.Web.Attributes
{
  
  
  public class Auth : ActionFilterAttribute
  {
    private string _cookieName;
    
    public string RequiredRole { get;set; }

    public Auth(string requiredRole, string cookieName) : this(requiredRole)
    {
      this._cookieName = cookieName;
    }

    public Auth(string requiredRole)
    {
      this.RequiredRole = requiredRole;
    }

    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
      var validRequest = true;
      
      var allowedAnnoyoumousRequest = filterContext.Controller.GetType().GetCustomAttributes(true).Any(x => x.GetType() == typeof(BBS.Libraries.Web.Attributes.Everyone));

      if (!allowedAnnoyoumousRequest)
      {
        allowedAnnoyoumousRequest = filterContext.ActionDescriptor.GetCustomAttributes(true).Any(action => action.GetType() == typeof(BBS.Libraries.Web.Attributes.Everyone));
      }
      if (allowedAnnoyoumousRequest)
      {
        return;
      }
      string cookieName = string.Empty;

      if (string.IsNullOrEmpty(_cookieName))
      {
        cookieName = FormsAuthentication.FormsCookieName;
      }
      else
      {
        cookieName = _cookieName;
      }

      var authCookie = filterContext.HttpContext.Request.Cookies[cookieName];

      if (authCookie == null)
      {
        validRequest = false;
      }
      else
      {
        var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
        var roles = authTicket.UserData.Split(new Char[] { '|' });

        var userIdentity = new GenericIdentity(authTicket.Name);
        var userPrincipal = new GenericPrincipal(userIdentity, roles);

        filterContext.HttpContext.User = userPrincipal;
      }

      if (!validRequest || !filterContext.HttpContext.User.IsInRole(this.RequiredRole) || !filterContext.HttpContext.User.Identity.IsAuthenticated)
      {
        var config = (System.Web.Configuration.CustomErrorsSection)System.Web.Configuration.WebConfigurationManager.GetSection("system.web/customErrors");
        if (config.Errors[((int)System.Net.HttpStatusCode.Forbidden).ToString()] != null)
        {
          string urlToRedirectTo = config.Errors[((int)System.Net.HttpStatusCode.Forbidden).ToString()].Redirect;
          filterContext.Result = new RedirectResult(urlToRedirectTo);
        }
        else
        {
          filterContext.Result = new HttpStatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
        }
      }
    }
  }
}
