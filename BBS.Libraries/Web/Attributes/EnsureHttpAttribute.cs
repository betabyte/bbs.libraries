﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BBS.Libraries.Web.Attributes
{
  public class EnsureHttpAttribute : FilterAttribute, IAuthorizationFilter
  {
    public void OnAuthorization(AuthorizationContext filterContext)
    {
      var request = filterContext.HttpContext.Request;
      
      var chainedSecureRequest = filterContext.ActionDescriptor.GetCustomAttributes(true).Any(action => action.GetType().Equals(typeof(BBS.Libraries.Web.Attributes.EnsureHttpsAttribute)));

      if (!chainedSecureRequest)
      {
        if (request.Url != null && request.IsSecureConnection && request.IsLocal)
        {
          filterContext.Result = new RedirectResult("http://" + request.Url.Host + string.Format(":{0}", Configuration.Current.PortElements.Single(x => !x.Secured).Port) + request.RawUrl);
        }
        if (request.Url != null && request.IsSecureConnection && !request.IsLocal)
        {
          filterContext.Result = new RedirectResult("http://" + request.Url.Host + request.RawUrl);
        }
      }
    }
  }
}
