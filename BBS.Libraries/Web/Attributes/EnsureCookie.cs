﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BBS.Libraries.Web.Attributes
{
  class EnsureCookie : ActionFilterAttribute
  {
    private string _cookieName { get; set; }

    public EnsureCookie(string CookieName)
    {
      _cookieName = CookieName;
    }

    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
      if (!BBS.Libraries.Web.Mvc.Cookie.Exists(_cookieName)) throw new HttpException(string.Format("Cookie {0} does not exist.", _cookieName));
    }
  }
}
