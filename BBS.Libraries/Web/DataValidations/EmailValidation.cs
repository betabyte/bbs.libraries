﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Web.DataValidations
{
    public class EmailValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var emailAddress = new BBS.Libraries.Emails.EmailAddress(value.ToString());
            return !emailAddress.IsValid ? new ValidationResult(base.ErrorMessage) : ValidationResult.Success;
        }
    }
}
