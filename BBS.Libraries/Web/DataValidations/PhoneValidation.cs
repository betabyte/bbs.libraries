﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Web.DataValidations
{
    public class PhoneValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var phone = new BBS.Libraries.Entities.Phone(value.ToString());
            return !phone.IsValid ? new ValidationResult(base.ErrorMessage) : ValidationResult.Success;
        }
    }
}
