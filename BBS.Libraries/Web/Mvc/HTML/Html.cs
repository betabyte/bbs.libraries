﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BBS.Libraries.Web.Mvc
{
  public static class Html
  {
    public static MvcHtmlString AddStyleSheet(this HtmlHelper input, string source)
    {
      var urlHelper = new UrlHelper(input.ViewContext.RequestContext);

      var returnString = string.Format("<link rel='stylesheet' href='{0}' type='text/css' />", urlHelper.Content(source));
      return new MvcHtmlString(returnString);
    }

    public static MvcHtmlString ImportStyleSheet(this HtmlHelper input, string source)
    {
      var urlHelper = new UrlHelper(input.ViewContext.RequestContext);

      var returnString = string.Format("@import url('{0}');", urlHelper.Content(source));
      return new MvcHtmlString(returnString);
    }

    public static MvcHtmlString AddJavaScript(this HtmlHelper input, string source)
    {
      var urlHelper = new UrlHelper(input.ViewContext.RequestContext);
      var returnString = string.Format("<script type='text/javascript' src='{0}'></script>", urlHelper.Content(source));
      return new MvcHtmlString(returnString);
    }

    public static MvcHtmlString Image(this HtmlHelper input, string source, string altText)
    {
      string img = "<img src='" + source + "' alt='" + altText + "'";

      img += " />";
      return new MvcHtmlString(img);
    }

    public static MvcHtmlString Label(string target, string text)
    {
      string label = "<label for='" + target + "'>" + text + "</label>";
      return new MvcHtmlString(label);
    }

    public static MvcHtmlString DatePicker(this HtmlHelper helper, string id, string name)
    {
      return new MvcHtmlString(string.Format("<input type='text' class='datepicker' id='{0}' name='{1}' />", id, name));
    }

    public static MvcHtmlString DatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> helper,
      Expression<Func<TModel, TProperty>> expression, string id)
    {
      var Name = expression.ToString().Split('.');
      if (Name.Length > 0)
        return
          new MvcHtmlString(string.Format("<input type='text' class='datepicker' id='{0}' name='{1}' />", id,
            Name[Name.Length - 1]));
      else return new MvcHtmlString(string.Format("<input type='text' class='datepicker' id='{0}' name='' />", id));
    }
  }
}
