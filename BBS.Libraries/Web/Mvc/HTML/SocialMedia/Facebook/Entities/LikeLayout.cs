﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Web.Mvc.HTML.SocialMedia.Facebook.Entities
{
  public enum LikeLayout
  {
    [BBS.Libraries.Enums.Info(Code = "standard")] Standard,
    [BBS.Libraries.Enums.Info(Code = "box_count")] BoxCount,
    [BBS.Libraries.Enums.Info(Code = "button_count")] ButtonCount,
    [BBS.Libraries.Enums.Info(Code = "button")] Button
  }
}
