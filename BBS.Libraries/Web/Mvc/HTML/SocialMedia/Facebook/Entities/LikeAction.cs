﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Web.Mvc.HTML.SocialMedia.Facebook.Entities
{
  public enum LikeAction
  {
    [BBS.Libraries.Enums.Info(Code = "fb-like")] Like, 
    [BBS.Libraries.Enums.Info(Code = "fb-recommend")] Recommend
  }
}
