﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Web.Mvc.HTML.SocialMedia.Facebook.Entities
{
  public enum ColorScheme
  {
    [BBS.Libraries.Enums.Info(Code = "light")]
    Light,
    [BBS.Libraries.Enums.Info(Code = "dark")]
    Dark
  }
}
