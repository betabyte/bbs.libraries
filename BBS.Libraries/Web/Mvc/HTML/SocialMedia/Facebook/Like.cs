﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BBS.Libraries.Web.Mvc.HTML.SocialMedia.Facebook.Entities;

namespace BBS.Libraries.Web.Mvc.HTML.SocialMedia.Facebook
{
  public static partial class Helpers
  {
    public static MvcHtmlString Like(this MvcHtmlString input, string Url, LikeAction action, LikeLayout layout, ColorScheme scheme, bool includeShareButton, bool showFaces, bool kidDirectedSite, string referralData)
    {
      var result = string.Format(@"<div class='fb-like' data-href='{0}' data-layout='{1}' data-action='{2}' data-show-faces='{3}' data-share='{4}' data-colorscheme='{5}' data-kid-directed-site='{6}' data-ref='{7}' width='{0}' ></div>", Url, BBS.Libraries.Enums<LikeLayout>.Code(layout), BBS.Libraries.Enums<LikeAction>.Code(action), showFaces, includeShareButton, BBS.Libraries.Enums<ColorScheme>.Code(scheme), kidDirectedSite, referralData);

      return new MvcHtmlString(result);
    }
  }
}
