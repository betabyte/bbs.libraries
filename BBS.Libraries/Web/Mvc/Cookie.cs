﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BBS.Libraries.Web.Mvc
{
  public static class Cookie
  {
    public static bool Exists(string cookieName)
    {
      var cookie = HttpContext.Current.Request.Cookies[cookieName];
      if (cookie != null)
      {
        return true;
      }
      return false;
    }

    public static void Kill(string cookieName, string domain = null, bool httpOnly = false, bool secure = false)
    {
      if (!Exists(cookieName)) return;

      var cookie = HttpContext.Current.Request.Cookies[cookieName];
      if (cookie != null)
      {
        cookie = new HttpCookie(cookieName)
        {
          Value = null,
          Expires = System.DateTime.Now.AddYears(-1),
          Domain = domain,
          HttpOnly = httpOnly,
          Name = cookieName,
          Secure = secure
        };
        HttpContext.Current.Response.Cookies.Add(cookie);
      }
    }

    public static void Set<T>(T obj, string cookieName, DateTime? expires, string domain = null, bool httpOnly = false, bool secure = false)
    {
      if (Exists(cookieName))
        Kill(cookieName, domain, httpOnly, secure);

      var cookie = HttpContext.Current.Request.Cookies[cookieName];

      var serializedCookie = Serialize(obj);

      cookie = new HttpCookie(cookieName)
      {
        Value = serializedCookie,
        Domain = domain,
        HttpOnly = httpOnly,
        Name = cookieName,
        Secure = secure
      };

      if (expires.HasValue)
      {
        cookie.Expires = expires.Value;
      }

      HttpContext.Current.Response.Cookies.Add(cookie);
    }

    public static string Serialize<T>(T obj)
    {
      var serializer = new BinaryFormatter();
      var result = string.Empty;
      using (var stream = new System.IO.MemoryStream())
      {
        serializer.Serialize(stream, obj);
        //Rewind...
        stream.Seek(0, SeekOrigin.Begin);
        var bytes = new byte[stream.Length];
        stream.Read(bytes, 0, (int)stream.Length);
        result = Convert.ToBase64String(bytes);
      }

      return result;
    }

    public static T Deserialize<T>(string cookieName)
    {
      if (!Exists(cookieName)) return default(T);

      var obj = HttpContext.Current.Request.Cookies[cookieName];

      var buffer = Convert.FromBase64String(obj.Value);

      T @class;

      using (var stream = new System.IO.MemoryStream(buffer))
      {
        var formatter = new BinaryFormatter();
        @class = (T)formatter.Deserialize(stream);
      }

      return @class;
    }
  }
}
