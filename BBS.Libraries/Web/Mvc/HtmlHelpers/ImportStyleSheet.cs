﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BBS.Libraries.Web.Mvc.HtmlHelpers
{
  public static partial class Extensions
  {
    public static MvcHtmlString ImportStyleSheet(this HtmlHelper input, string source)
    {
      var urlHelper = new UrlHelper(input.ViewContext.RequestContext);

      var returnString = string.Format("@import url('{0}');", urlHelper.Content(source));
      return new MvcHtmlString(returnString);
    }
  }
}
