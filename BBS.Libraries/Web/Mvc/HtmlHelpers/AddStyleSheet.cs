﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BBS.Libraries.Web.Mvc.HtmlHelpers
{
  public static partial class Extensions
  {
    public static MvcHtmlString AddStyleSheet(this HtmlHelper input, string source)
    {
      var urlHelper = new UrlHelper(input.ViewContext.RequestContext);

      var returnString = string.Format("<link rel='stylesheet' href='{0}' type='text/css' />", urlHelper.Content(source));
      return new MvcHtmlString(returnString);
    }
  }
}
