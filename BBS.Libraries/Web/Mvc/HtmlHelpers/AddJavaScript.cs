﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BBS.Libraries.Web.Mvc.HtmlHelpers
{
    public static partial class Extensions
    {
        public static MvcHtmlString AddJavaScript(this HtmlHelper input, string source, bool isTypescript = false)
        {
            // If the source is a typescript file, we need to change the extension
            if (isTypescript)
            {
                source = source.Replace(".ts", ".js");
            }
            var urlHelper = new UrlHelper(input.ViewContext.RequestContext);
            var returnString = string.Format("<script type='text/javascript' src='{0}'></script>",
                urlHelper.Content(source));
            return new MvcHtmlString(returnString);
        }
    }
}
