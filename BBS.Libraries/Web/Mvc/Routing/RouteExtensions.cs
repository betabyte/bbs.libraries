﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace BBS.Libraries.Web.Mvc.Routing
{
  public static class RouteExtensions
  {
    private static List<Type> _validTypes = new List<Type>()
                         {
                           typeof(ActionResult),
                           typeof(FileContentResult),
                           typeof(JsonResult), 
                           typeof(JavaScriptResult),
                           typeof(ContentResult),
                           typeof(EmptyResult),
                         };
    /// <summary>
    /// Generates a new routename.  This is based on Guid.NewGuid().
    /// </summary>
    /// <param name="routeNames"></param>
    public static void AssignRouteNames(this IRouteNames routeNames)
    {
      var mType = routeNames.GetType();

      var fields = mType.GetFields();

      foreach (var field in fields)
      {
        if (!field.IsLiteral && field.GetValue(mType) != string.Empty)
        {
          var guid = new Guid();
          guid = Guid.NewGuid();
          field.SetValue(mType, guid.ToString());
        }
      }
    }

    #region Defaults

    /// <summary>
    /// Sets up the default routing parameters.
    /// </summary>
    /// <param name="routes"></param>
    /// <returns></returns>
    public static Route MapDefaultRoute(this RouteCollection routes)
    {
      return routes.MapRoute("Default", "{controller}/{action}",
                             new { controller = "Home", action = "Index" });
    }

    /// <summary>
    /// Sets up the default route and sends you off to the correct link
    /// </summary>
    /// <param name="routes"></param>
    /// <param name="routeName"></param>
    /// <returns></returns>
    public static Route MapDefaultRoute(this RouteCollection routes, string routeName)
    {
      var r = RouteTable.Routes.OfType<System.Web.Routing.Route>();

      var route = r.FirstOrDefault(y => y.DataTokens != null && y.DataTokens["__RouteName"] != null && y.DataTokens["__RouteName"].ToString() == routeName);
      if (route != null)
      {
        var defaultController = route.Defaults["controller"];
        var defaultAction = route.Defaults["action"];

        routes.Remove(route);
        
        return routes.MapRoute(routeName, string.Empty, new { controller = defaultController, action = defaultAction });
      }
      else
      {
        throw new ArgumentException(string.Format("Route not found for routeName {0}", routeName));
      }
    }

    public static Route MapDefaultRoute(this RouteCollection routes, string Controller, string Action)
    {
      return routes.MapRoute("Default", "{controller}/{action}",
                             new { controller = Controller, action = Action });
    }

    public static Route MapDefaultAreaRoute(this RouteCollection routes, string Area, string Controller, string Action)
    {
      return routes.MapRoute("Default", "{area}/{controller}/{action}", new { area = Area, controller = Controller, action = Action });
    }

    #endregion

    public static Route MapRoute<TController>(this RouteCollection routes, string Name, string Url, Expression<Func<TController, ActionResult>> action, object defaults = null, IRouteConstraint contstraints = null) where TController : IController
    {
      var methodCallExpression = (MethodCallExpression)action.Body;

      if (!IsValidActionType(methodCallExpression.Method.ReturnType))
      {
        throw new ArgumentException(string.Format("ControllerAction method {0} does not return Result", methodCallExpression.Method.Name));
      }
      if (string.IsNullOrEmpty(Name))
      {
        Name = typeof(TController).Name + methodCallExpression.Method.Name;
      }
      if (string.IsNullOrEmpty(Url))
      {
        Url = methodCallExpression.Method.Name.ToLower() == "index" ? string.Format("{0}", typeof (TController).Name.RemoveLastInstanceOf("Controller")) : string.Format("{0}/{1}", typeof (TController).Name.RemoveLastInstanceOf("Controller"), methodCallExpression.Method.Name);
      }
      if (defaults == null)
      {
        defaults = new { action = methodCallExpression.Method.Name };
      }
      else
      {
        defaults = new RouteValueDictionary(defaults);
        (defaults as RouteValueDictionary).Add("action", methodCallExpression.Method.Name);
      }
      return routes.MapRoute<TController>(Name, Url, defaults, contstraints);
    }

    public static bool IsValidActionType(Type type)
    {
      return (_validTypes.IndexOf(type) >= 0);
    }

    private static Route MapRoute<TController>(this RouteCollection routes, string name, string url, object defaults, IRouteConstraint constraints) where TController : IController
    {
      var route = new Route(url, new MvcRouteHandler())
      {
        Defaults = (defaults is RouteValueDictionary) ? (defaults as RouteValueDictionary) : new RouteValueDictionary(defaults),
        Constraints = new RouteValueDictionary(constraints)
      };
      string controller = typeof(TController).Name.RemoveLastInstanceOf("Controller");
      if (route.Defaults.ContainsKey("controller"))
      {
        throw new ArgumentException("Defaults contain key 'Controller', but using a strongly typed route");
      }
      else
      {
        route.Defaults.Add("controller", controller);
      }
      object action = null;
      if (route.Defaults.TryGetValue("action", out action))
      {
        if (action.GetType().IsGenericType && action.GetType().GetGenericTypeDefinition() == typeof(ControllerAction<>))
        {
          route.Defaults.Add("controllerAction", route.Defaults["action"]);
          route.Defaults["action"] = route.Defaults["controllerAction"].ToString();
        }
      }
      if (!string.IsNullOrWhiteSpace(name))
      {
        route.DataTokens = new RouteValueDictionary();
        route.DataTokens["__RouteName"] = name;
      }
      routes.Add(name, route);
      return route;
    }

    //TODO:  Bring this into the Strings utils.
    public static string RemoveLastInstanceOf(this string text, string textToRemove)
    {
      return text.Remove(text.LastIndexOf(textToRemove));
    }
  }
}
