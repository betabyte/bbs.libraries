﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BBS.Libraries.Web.Mvc.Routing
{
  public class ControllerAction<TController> where TController : IController
  {
    public string Action { get; private set; }
    public override string ToString()
    {
      return Action;
    }
    public static implicit operator string(ControllerAction<TController> controllerAction)
    {
      return controllerAction.Action;
    }
    public ControllerAction(Expression<Func<TController, ActionResult>> action)
    {
      var methodCallExpression = (MethodCallExpression)action.Body;
      if (!RouteExtensions.IsValidActionType(methodCallExpression.Method.ReturnType))
      {
        throw new ArgumentException(string.Format("ControllerActionMethod {0} does not return type Result", methodCallExpression.Method.Name));
      }
      Action = methodCallExpression.Method.Name;
    }
  }
}
