﻿using System;
using System.Configuration;

namespace BBS.Libraries
{
  class Configuration : AbstractConfiguration
  {
    public class Keys
    {
      public const string CurrentConfigurationName = "bbs.libraries";
      public const string Ports = "ports";
      public const string PortAdd = "port";

      public const string MefParts = "mefParts";
    }

    public static readonly Configuration Current = GetConfiguration<Configuration>(Keys.CurrentConfigurationName);

    #region MefParts
    [ConfigurationProperty(Keys.MefParts)]
    [ConfigurationCollection(typeof(MefPart))]
    public GenericConfigurationElementCollection<MefPart> MefParts
    {
        get { return (GenericConfigurationElementCollection<MefPart>)this[Keys.MefParts]; }
    }

    public class MefPart : ConfigurationElement
    {
        public class Keys
        {
            public const string FileLocation = "fileLocation";
        }

        [ConfigurationProperty(Keys.FileLocation, IsRequired = true)]
        public string FileLocation
        {
            get { return (String)this[Keys.FileLocation]; }
            set { this[Keys.FileLocation] = value; }
        }
    }
    #endregion

    #region Ports
    [ConfigurationProperty(Keys.Ports)]
    [ConfigurationCollection(typeof(Ports), AddItemName = Keys.PortAdd)]
    public GenericConfigurationElementCollection<Ports> PortElements
    {
      get { return (GenericConfigurationElementCollection<Ports>)this[Keys.Ports]; }
    }

    public class Ports : ConfigurationElement
    {
      public class Keys
      {
        public const string PortNumber = "portNumber";
        public const string Secured = "secure";
      }

      [ConfigurationProperty(Keys.Secured, IsRequired = true)]
      public bool Secured
      {
        get { return (bool)this[Keys.Secured];}
        set { this[Keys.Secured] = value;}
      }

      [ConfigurationProperty(Keys.PortNumber, IsRequired = true)]
      public String Port
      {
        get { return (String)this[Keys.PortNumber]; }
        set { this[Keys.PortNumber] = value; }
      }
    }
    #endregion
  }
}
