﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries
{
  public abstract class AbstractConfiguration : ConfigurationSection
  {
    public static T GetConfiguration<T>(string sectionName)
    {
      return (T)System.Configuration.ConfigurationManager.GetSection(sectionName);
    }
  }
}
