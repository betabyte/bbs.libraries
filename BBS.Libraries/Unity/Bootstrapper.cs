﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MefContrib.Integration.Unity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace BBS.Libraries.Unity
{
    public static class Bootstrapper
    {
        private static IUnityContainer _container { get; set; }
        static Bootstrapper()
        {
            _container = new UnityContainer();
        }

        public static IUnityContainer BuildUnityContainer()
        {

            _container.RegisterInstance<IUnityContainer>(_container);

            RegisterContainerInstancesFromConfig(_container);

            return _container;

        }

        public static IUnityContainer BuildUnityContainer(AggregateCatalog catalog)
        {
            _container.RegisterCatalog(catalog);
            _container.RegisterInstance<IUnityContainer>(_container);

            RegisterContainerInstancesFromConfig(_container);

            return _container;
        }

        private static void RegisterContainerInstancesFromConfig(IUnityContainer container, string sectionName = "unity")
        {
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            section.Containers.Default.Configure(container);
        }
    }
}
