﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace BBS.Libraries.Security
{
  [DataContract]
  public class User
  {
    [DataMember]
    public Guid? UserId { get; set; }
    [DataMember]
    public string UserName { get; set; }
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }
    [DataMember]
    public BBS.Libraries.Emails.EmailAddress EmailAddress { get; set; }
    [DataMember]
    public bool IsActive { get; set; }
    [DataMember]
    public RoleCollection Roles { get; set; }
    [DataMember]
    public GroupCollection Groups { get; set; }
    
    public string RoleNames
    {
      get
      {
        var resultList = new List<string>();
        var result = string.Empty;

        foreach (var item in Groups)
        {
          resultList.AddRange(item.Roles.Select(role => role.RoleName));
        }

        resultList.AddRange(Roles.Where(x => !resultList.Contains(x.RoleName)).Select(role => role.RoleName));

        result = resultList.Aggregate(result,
          (current, item) =>
            item == resultList.First() ? string.Format("{0}", item) : string.Format("{0}|{1}", current, item));

        return result;
      }
    }

    public User()
    {
      Groups = new GroupCollection();
      Roles = new RoleCollection();
    }
  }
}
