﻿using System;
using System.Runtime.Serialization;


namespace BBS.Libraries.Security
{
  [DataContract]
  public class Group
  {
    [DataMember]
    public Guid? GroupId { get; set; }
    [DataMember]
    public string Name { get; set; }
    
    [DataMember]
    public RoleCollection Roles { get; set; }
  }
}