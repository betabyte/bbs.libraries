﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Entities.Security
{
  [Serializable]
    public enum PermissionLevels
    {
        None = 0,
        User = 100,
        Supervisor = 200,
        Manager = 300,
        Director = 400,
        VP = 500,
        Executive = 600,
        Administrator = 900,
        God = 99999
    }

}
