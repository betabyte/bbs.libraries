﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Security
{
  [CollectionDataContract]
  public class UserCollection : List<User>
  {
    public User Find(Guid userId)
    {
      return this.Find(x => x.UserId == userId);
    }
  }
}
