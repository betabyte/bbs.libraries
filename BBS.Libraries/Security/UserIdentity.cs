using System;
using System.Security.Principal;

namespace BBS.Libraries.Security
{
  public abstract class UserIdentity : GenericIdentity
  {
    public User User { get; set; }

    public UserIdentity(User user) : base(user.UserName)
    {
      this.User = user;
    }
  }


}