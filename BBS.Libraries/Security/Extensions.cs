﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace BBS.Libraries.Security
{
  public static class Extensions
  {
    public static string UserAutheticationToken(this BBS.Libraries.Security.User user)
    {
      //var userIdentity = new BBS.Libraries.Security.UserIdentity(user);
      //var principle = new BBS.Libraries.Security.UserPrinciple(userIdentity);

      //HttpContext.Current.User = principle;

      var roles = user.RoleNames;

      var authTicket = new FormsAuthenticationTicket(1, user.UserName, DateTime.Now, DateTime.Now.AddMinutes(15), false, roles);
      return FormsAuthentication.Encrypt(authTicket);
    }

    public static User FromAuthenticationToken(this string token)
    {
      var ticket = FormsAuthentication.Decrypt(token);
      
      var user = new BBS.Libraries.Security.User(){UserName = ticket.Name};

      return user;
    }
  }
}
