﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BBS.Libraries.Security
{
  [DataContract]
  public class Role
  {
    [DataMember]
    public Guid? RoleId { get; set; }
    [DataMember]
    public string RoleName { get; set; }
    
    public Role()
    {
    }

    public Role(Enum enEnum)
    {
      RoleName = enEnum.ToString();
    }
  }
}