﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Security;

namespace BBS.Libraries.Security
{
    [CollectionDataContract]
    public class GroupCollection : List<Group>
    {
    }
}
