﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Web.Security;

namespace BBS.Libraries.Security
{
  public abstract class UserPrinciple : GenericPrincipal
  {
    public UserPrinciple(BBS.Libraries.Security.UserIdentity user) : base(user, user.User.Roles.Select(x => x.RoleName).ToArray())
    {      
    }
  }
}