﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Contracts
{
  public interface ICommand : IDbCommand
  {
    BBS.Libraries.Contracts.IDataReader Reader { get; set; }
    void Clear();
    void Append(string value);
    void AppendFormat(string value, params  object[] values);
    bool EndOfWhere();
    bool EndOfWhere(string replaceWith);
    bool EndOfList();
    bool EndOfList(string replaceWith);
  }
}
