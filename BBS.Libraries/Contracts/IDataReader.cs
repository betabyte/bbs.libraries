﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Contracts
{
  public interface IDataReader : System.Data.IDataReader
  {
    string GetName(string target);
    Type GetFieldType(string target);
    
    bool GetBoolean(string target);
    byte[] GetBytes(string target);
    DateTime GetDateTime(string target);
    DateTime? GetDateTimeNullable(string target);
    Guid GetGuid(string target);
    Guid? GetGuidNullable(string target);
    int GetInt(string target);
    int? GetIntNullable(string target);
    float GetFloat(string target);
    float? GetFloatNullable(string target);
    double GetDouble(string target);
    double? GetDoubleNullable(string target);
    string GetString(string target);
    bool IsDbNull(string target);
    T GetEnum<T>(string target);
  }
}
