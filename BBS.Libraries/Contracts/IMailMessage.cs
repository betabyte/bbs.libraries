﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Emails;

namespace BBS.Libraries.Contracts
{
  internal interface IMailMessage
  {
    [DataMember] EmailAddress From { get; set; }

    [DataMember] EmailAddress Sender { get; set; }

    [DataMember] EmailAddressCollection ReplyToList { get; set; }

    [DataMember] EmailAddressCollection To { get; set; }

    [DataMember] EmailAddressCollection CC { get; set; }

    [DataMember] EmailAddressCollection Bcc { get; set; }

    [DataMember] MailPriority Priority { get; set; }

    [DataMember] DeliveryNotificationOptions DeliveryNotificationOptions { get; set; }

    [DataMember] string Subject { get; set; }

    [DataMember] int? SubjectEncoding { get; set; }

    [IgnoreDataMember] NameValueCollection Headers { get; }

    [DataMember] int? HeadersEncoding { get; set; }

    [DataMember] string Body { get; set; }

    [DataMember] int? BodyEncoding { get; set; }

    [DataMember] bool IsBodyHtml { get; set; }

    [DataMember] MailMessageAttachmentCollection Attachments { get; set; }

    [DataMember] MailMessageAlternateViewCollection AlternateViews { get; set; }
  }
}
