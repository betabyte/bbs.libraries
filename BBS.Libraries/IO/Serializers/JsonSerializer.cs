﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.IO.Serializers
{
    public partial class JsonSerializer<T> : Serializer<T> where T : class, new()
    {
        public override string Serialize(object @object)
        {
            var result = string.Empty;

            using (var stream = new System.IO.MemoryStream())
            {
                var serializer = new DataContractJsonSerializer(typeof (T));
                serializer.WriteObject(stream, @object);
                stream.Position = 0;
                using (var reader = new System.IO.StreamReader(stream))
                {
                    result = string.Format("{0}", reader.ReadToEnd());
                }
            }
            return result;
        }

        public override T Deserialize(string value, List<Type> knownTypes)
        {
            throw new NotImplementedException();
        }

        public override T Deserialize(string value)
        {
            var result = new T();

            byte[] byteArray = Encoding.UTF8.GetBytes(value);

            using (var stream = new System.IO.MemoryStream(byteArray))
            {
                var serializer = new DataContractJsonSerializer(typeof (T));
                stream.Position = 0;
                result = serializer.ReadObject(stream) as T;
            }
            return result;
        }
    }
}