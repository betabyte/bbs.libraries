﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.IO.Manipulators
{
  public class FileManipulatorFactory
  {
    public static IFileManipulator GetManipulator(BBS.Libraries.IO.File.FileLocationType fileLocationType)
    {
      if (fileLocationType == File.FileLocationType.FTP)
      {
        return new Ftp();
      }
      if (fileLocationType == File.FileLocationType.Local || fileLocationType == File.FileLocationType.UNC)
      {
        return new Windows();
      }
      if (fileLocationType == File.FileLocationType.Http)
      {
        return new Http();
      }

      throw new ArgumentException("Incorrect file type.");
    }
  }
}
