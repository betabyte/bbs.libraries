﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using BBS.Libraries.IO;

namespace BBS.Libraries.Extensions
{
  public static partial class Object
  {
    public static string ToXmlString<T>(this object o) where T : class, new() 
    {
      var serializer = new BBS.Libraries.IO.Serializers.XmlSerializer<T>();

      return serializer.Serialize(o);
    }
  }
}
