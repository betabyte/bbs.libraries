﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;
using Microsoft.Practices.Unity;

namespace BBS.Libraries.Repository
{
    public class DataRepositoryFactory : IDataRepositoryFactory
    {
        private IUnityContainer _container;

        public DataRepositoryFactory(IUnityContainer container)
        {
            _container = container;
        }

        public T GetDataRepository<T>() where T : IDataRepository
        {
            return _container.Resolve<T>();
        }
    }
}
