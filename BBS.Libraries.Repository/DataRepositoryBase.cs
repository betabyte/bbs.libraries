﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;

namespace BBS.Libraries.Repository
{
    public abstract class DataRepositoryBase<T, U> : IDataRepository<T>
        where T : class, IIdentifiableEntity, new()
        where U : DbContext, new()
    {
        protected abstract T AddEntity(U entityContext, T entity);
        protected abstract T UpdateEntity(U entityContext, T entity);
        protected abstract IEnumerable<T> GetEntities(U entityContext);
        protected abstract T GetEntity(U entityContext, Func<T, bool> queryToRun);
        protected abstract IEnumerable<T> GetEntities(U entityContext, Func<T, bool> queryToRun);

        protected virtual T RemoveEntity(U entityContext, T entity)
        {
            return entity;
        }


        public T Add(T entity)
        {
            using (var entityContext = new U())
            {
                T addedEntity = AddEntity(entityContext, entity);
                entityContext.SaveChanges();
                return addedEntity;
            }
        }
        public void Remove(T entity)
        {
            using (var entityContext = new U())
            {
                T removedEntity = RemoveEntity(entityContext, entity);
                entityContext.Entry<T>(removedEntity).State = EntityState.Deleted;
                entityContext.SaveChanges();
            }
        }
        public void Remove(Func<T, bool> queryToRun)
        {
            using (var entityContext = new U())
            {
                T entity = GetEntity(entityContext, queryToRun);

                T removedEntity = RemoveEntity(entityContext, entity);

                entityContext.Entry<T>(removedEntity).State = EntityState.Deleted;

                entityContext.SaveChanges();
            }
        }
        public T Update(T entity)
        {
            using (var entityContext = new U())
            {
                T existingEntity = UpdateEntity(entityContext, entity);

                SimpleMapper.PropertyMap(entity, existingEntity);

                entityContext.SaveChanges();
                return existingEntity;
            }
        }
        public IEnumerable<T> Get()
        {
            using (var entityContext = new U())
            {
                return GetEntities(entityContext).ToList();
            }
        }
        public T Get(Func<T, bool> querytToRun)
        {
            using (var entityContext = new U())
            {
                return GetEntity(entityContext, querytToRun);
            }
        }
        public IEnumerable<T> GetEntities(Func<T, bool> queryToRun)
        {
            using (var entityContext = new U())
            {
                return GetEntities(entityContext, queryToRun);
            }
        }
    }
}
