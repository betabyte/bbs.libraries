﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;
using BBS.Libraries.IO;
using BBS.Libraries.Razor;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using TuesPechkin;

namespace BBS.Libraries.Documents.PDF
{
  public class RazorContentGenerator<T> : RazorGenerator<T> where T : class, IRazorContentModel
  {
    public static ITemplateService _templateService;

    public ITemplateService TemplateService
    {
      get
      {
        ITemplateService templateService = _templateService;
        if (_templateService != null)
        {
          return _templateService;
        }
        return _templateService = (ITemplateService)new TemplateService((ITemplateServiceConfiguration)new TemplateServiceConfiguration()
        {
          Resolver = (ITemplateResolver)new VirtualFileResolver(this.NamespaceViewName),
        });
      }
    }

    public virtual string FileViewFileName { get; set; }
    public string FileOutputLocation { get; private set; }

    public RazorContentGenerator(string namespaceViewName, string fileViewFileName, string fileOutputLocation)
      : base(namespaceViewName)
    {
      this.FileViewFileName = fileViewFileName;
      this.FileOutputLocation = fileOutputLocation;
    }

    public byte[] File(IRazorContentModel model)
    {
      string str = string.Empty;
      if (!string.IsNullOrWhiteSpace(this.FileViewFileName))
      {
        str = this.TemplateService.Resolve(this.FileViewFileName, (object)model).Run(new ExecuteContext());
      }
      var converter = Factory.Create();

      return converter.Convert(str);
    }

    public override void Generate(T model)
    {
      BBS.Libraries.IO.File.WriteFile(FileOutputLocation, File(model));
    }
  }
}
