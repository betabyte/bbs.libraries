﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Documents
{
  public static partial class DocumentExtensions
  {
    public static string ToDocumentMimeType(this DocumentType value)
    {
      switch (value)
      {
          case DocumentType.Jpg:
            return Document.MimeTypes.Jpg;
            break;
          case DocumentType.Png:
            return Document.MimeTypes.Png;
            break;
          case DocumentType.Xls:
            return Document.MimeTypes.Xls;
            break;
          case DocumentType.Xlsx:
            return Document.MimeTypes.Xlsx;
            break;
          default:
            return string.Empty;
      }
    }
  }
}
