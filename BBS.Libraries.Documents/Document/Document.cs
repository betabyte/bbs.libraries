﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using BBS.Libraries.Extensions;

namespace BBS.Libraries.Documents
{
  public partial class Document
  {
    public Guid? Id { get; set; }
    public string MimeType { get { return this.DocumentType.ToDocumentMimeType(); } }
    public string Name { get; set; }
    public byte[] Bytes { get; set; }
    public DocumentType DocumentType { get; set; }    
  }
}
