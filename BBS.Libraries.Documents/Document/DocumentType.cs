﻿namespace BBS.Libraries.Documents
{
  public enum DocumentType
  {
    Docx,
    Xlsx,
    Xls,
    Jpg,
    Bmp,
    Png
  }
}