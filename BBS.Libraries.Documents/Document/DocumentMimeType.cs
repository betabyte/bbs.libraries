﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Documents
{
  public partial class Document
  {
    public static class MimeTypes
    {
      public static string Jpg = "image/jpeg";
      public static string Png = "image/png";
      public static string Svg = "image/svg+xml";
      public static string Gif = "image/gif";

      public static string Css = "text/css";
      public static string Csv = "text/csv";
      public static string Txt = "text/plain";
      public static string Xml = "text/xml";

      public static string Pdf = "application/pdf";
      public static string Zip = "application/zip";

      public static string Xls = "application/vnd.ms-excel";
      public static string Xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }
  }
}
