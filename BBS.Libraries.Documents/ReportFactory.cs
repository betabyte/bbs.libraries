﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBS.Libraries.Contracts;
using Microsoft.Practices.Unity;

namespace BBS.Libraries.Documents
{
    public class ReportFactory : IReportFactory
    {
        private IUnityContainer _container;

        public ReportFactory(IUnityContainer container)
        {
            _container = container;
        }

        public T GetReportRepository<T>() where T : IReport
        {
            return _container.Resolve<T>();
        }
    }
}
