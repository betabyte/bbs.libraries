﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Style 
    {
      public Style()
      {
        Font = new Font();
        Alignment = new Alignment();
        Border = new Border();
        Fill = new Fill();
        Protection = new Protection();
        CellFormat = new CellFormat();
        
      }
      
      public Alignment Alignment { get; set; }
      public Border Border { get; set; }
      public Fill Fill { get; set; }
      public Font Font { get; set; }
      public CellFormat CellFormat { get; set; }
      public Protection Protection { get; set; }
    }
  }
}
