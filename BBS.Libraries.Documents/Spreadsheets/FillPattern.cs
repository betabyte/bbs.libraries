﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public enum FillPattern
    {
      DarkDown,
      DarkGray,
      DarkGrid,
      DarkHorizontal,
      DarkTrellis,
      DarkUp,
      DarkVertical,
      Gray0625,
      Gray125,
      LightDown,
      LightGray,
      LightGrid,
      LightHorizontal,
      LightTrellis,
      LightUp,
      LightVertical,
      MediumGray,
      None,
      Solid,
    }
  }
}