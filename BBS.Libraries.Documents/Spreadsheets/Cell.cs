﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
    public partial class Spreadsheets
    {
        public class Cell : ClosedXML.Excel.IXLCell
        {
            private ClosedXML.Excel.IXLCell _cell { get; set; }

            public Cell(IXLCell cell)
            {
                _cell = cell;
            }

            public void SetStyle(Style style)
            {
                #region protection

                _cell.Style.Protection.SetHidden(style.Protection.Hidden);
                _cell.Style.Protection.SetLocked(style.Protection.Locked);

                #endregion

                #region NumberFormat

                _cell.Style.NumberFormat.SetFormat(style.CellFormat.Format);

                if (style.CellFormat.CellFormatId.HasValue)
                {
                    _cell.Style.NumberFormat.SetNumberFormatId(style.CellFormat.CellFormatId.Value);
                }

                #endregion

                #region DateFormat

                _cell.Style.DateFormat.SetFormat(style.CellFormat.Format);

                if (style.CellFormat.CellFormatId.HasValue)
                {
                    _cell.Style.DateFormat.SetNumberFormatId(style.CellFormat.CellFormatId.Value);
                }

                #endregion

                #region Fill

                _cell.Style.Fill.SetBackgroundColor(style.Fill.BackgroundColor.ToXlColor());
                _cell.Style.Fill.SetPatternBackgroundColor(style.Fill.PatternBackgroundColor.ToXlColor());
                _cell.Style.Fill.SetPatternColor(style.Fill.PatternColor.ToXlColor());
                _cell.Style.Fill.SetPatternType(style.Fill.PatternType.ToXLFillPatternValues());

                #endregion

                #region Border

                _cell.Style.Border.SetBottomBorder(style.Border.BottomBorder.ToXLBorderStyleValues());
                _cell.Style.Border.SetBottomBorderColor(style.Border.BottomBorderColor.ToXlColor());

                _cell.Style.Border.SetLeftBorder(style.Border.LeftBorder.ToXLBorderStyleValues());
                _cell.Style.Border.SetLeftBorderColor(style.Border.LeftBorderColor.ToXlColor());

                _cell.Style.Border.SetRightBorder(style.Border.RightBorder.ToXLBorderStyleValues());
                _cell.Style.Border.SetRightBorderColor(style.Border.RightBorderColor.ToXlColor());

                _cell.Style.Border.SetTopBorder(style.Border.TopBorder.ToXLBorderStyleValues());
                _cell.Style.Border.SetTopBorderColor(style.Border.TopBorderColor.ToXlColor());

                _cell.Style.Border.SetDiagonalBorder(style.Border.DiagonalBorder.ToXLBorderStyleValues());
                _cell.Style.Border.SetDiagonalBorderColor(style.Border.DiagonalBorderColor.ToXlColor());

                _cell.Style.Border.SetDiagonalDown(style.Border.DiagonalDown);
                _cell.Style.Border.SetDiagonalUp(style.Border.DiagonalUp);

                #endregion

                #region Alignment

                _cell.Style.Alignment.SetHorizontal(style.Alignment.Horizontal.ToXLAlignmentHorizontalValues());
                _cell.Style.Alignment.SetIndent(style.Alignment.Indent);
                _cell.Style.Alignment.SetJustifyLastLine(style.Alignment.JustifyLastLine);
                _cell.Style.Alignment.SetReadingOrder(style.Alignment.ReadingOrder.ToXLAlignmentReadingOrderValues());
                _cell.Style.Alignment.SetRelativeIndent(style.Alignment.RelativeIndent);
                _cell.Style.Alignment.SetShrinkToFit(style.Alignment.ShrinkToFit);
                _cell.Style.Alignment.SetTextRotation(style.Alignment.TextRotation);
                _cell.Style.Alignment.SetTopToBottom(style.Alignment.TopToBottom);
                _cell.Style.Alignment.SetVertical(style.Alignment.Vertical.ToXLAlignmentVerticalValues());
                _cell.Style.Alignment.SetWrapText(style.Alignment.WrapText);

                #endregion

                #region Font

                _cell.Style.Font.SetBold(style.Font.Bold);
                _cell.Style.Font.SetFontColor(style.Font.Color.ToXlColor());
                _cell.Style.Font.SetFontFamilyNumbering(style.Font.FamilyNumbering.ToXlFontFamilyNumberingValues());
                _cell.Style.Font.SetFontName(style.Font.FontName);
                _cell.Style.Font.SetFontSize(style.Font.Size);
                _cell.Style.Font.SetItalic(style.Font.Italic);
                _cell.Style.Font.SetShadow(style.Font.Shadow);
                _cell.Style.Font.SetStrikethrough(style.Font.Strikethrough);
                _cell.Style.Font.SetUnderline(style.Font.Underline.ToXlFontUnderlineValues());
                _cell.Style.Font.SetVerticalAlignment(style.Font.VerticalAlignment.ToXlFontVerticalTextAlignmentValues());

                #endregion
            }

            public IXLCell SetDataType(XLCellValues dataType)
            {
                return _cell.SetDataType(dataType);
            }

            public IXLCell SetValue<T>(T value)
            {
                return _cell.SetValue<T>(value);
            }

            public T GetValue<T>()
            {
                return _cell.GetValue<T>();
            }

            public string GetString()
            {
                return _cell.GetString();
            }

            public string GetFormattedString()
            {
                return _cell.GetFormattedString();
            }

            public double GetDouble()
            {
                return _cell.GetDouble();
            }

            public bool GetBoolean()
            {
                return _cell.GetBoolean();
            }

            public DateTime GetDateTime()
            {
                return _cell.GetDateTime();
            }

            public TimeSpan GetTimeSpan()
            {
                return _cell.GetTimeSpan();
            }

            public XLHyperlink GetHyperlink()
            {
                return _cell.GetHyperlink();
            }

            public bool TryGetValue<T>(out T value)
            {
                return _cell.TryGetValue<T>(out value);
            }

            public IXLCell Clear(XLClearOptions clearOptions = XLClearOptions.ContentsAndFormats)
            {
                return _cell.Clear(clearOptions);
            }

            public void Delete(XLShiftDeletedCells shiftDeleteCells)
            {
                _cell.Delete(shiftDeleteCells);
            }

            public IXLCell SetFormulaA1(string formula)
            {
                return _cell.SetFormulaA1(formula);
            }

            public IXLCell SetFormulaR1C1(string formula)
            {
                return _cell.SetFormulaR1C1(formula);
            }

            public IXLRange AsRange()
            {
                return _cell.AsRange();
            }

            public IXLRange InsertData(IEnumerable data)
            {
                return _cell.InsertData(data);
            }

            public IXLTable InsertTable<T>(IEnumerable<T> data)
            {
                return _cell.InsertTable<T>(data);
            }

            public IXLTable InsertTable<T>(IEnumerable<T> data, bool createTable)
            {
                return _cell.InsertTable<T>(data, createTable);
            }

            public IXLTable InsertTable<T>(IEnumerable<T> data, string tableName)
            {
                return _cell.InsertTable<T>(data, tableName);
            }

            public IXLTable InsertTable<T>(IEnumerable<T> data, string tableName, bool createTable)
            {
                throw new NotImplementedException();
            }

            public IXLTable InsertTable(DataTable data)
            {
                throw new NotImplementedException();
            }

            public IXLTable InsertTable(DataTable data, bool createTable)
            {
                throw new NotImplementedException();
            }

            public IXLTable InsertTable(DataTable data, string tableName)
            {
                throw new NotImplementedException();
            }

            public IXLTable InsertTable(DataTable data, string tableName, bool createTable)
            {
                throw new NotImplementedException();
            }

            public IXLDataValidation SetDataValidation()
            {
                throw new NotImplementedException();
            }

            public IXLCells InsertCellsAbove(int numberOfRows)
            {
                throw new NotImplementedException();
            }

            public IXLCells InsertCellsBelow(int numberOfRows)
            {
                throw new NotImplementedException();
            }

            public IXLCells InsertCellsAfter(int numberOfColumns)
            {
                throw new NotImplementedException();
            }

            public IXLCells InsertCellsBefore(int numberOfColumns)
            {
                throw new NotImplementedException();
            }

            public IXLCell AddToNamed(string rangeName)
            {
                throw new NotImplementedException();
            }

            public IXLCell AddToNamed(string rangeName, XLScope scope)
            {
                throw new NotImplementedException();
            }

            public IXLCell AddToNamed(string rangeName, XLScope scope, string comment)
            {
                throw new NotImplementedException();
            }

            public IXLCell CopyFrom(IXLCell otherCell)
            {
                throw new NotImplementedException();
            }

            public IXLCell CopyFrom(string otherCell)
            {
                throw new NotImplementedException();
            }

            public IXLCell CopyTo(IXLCell target)
            {
                throw new NotImplementedException();
            }

            public IXLCell CopyTo(string target)
            {
                throw new NotImplementedException();
            }

            public bool IsMerged()
            {
                throw new NotImplementedException();
            }

            public bool IsEmpty()
            {
                throw new NotImplementedException();
            }

            public bool IsEmpty(bool includeFormats)
            {
                throw new NotImplementedException();
            }

            public IXLCell CellAbove()
            {
                throw new NotImplementedException();
            }

            public IXLCell CellAbove(int step)
            {
                throw new NotImplementedException();
            }

            public IXLCell CellBelow()
            {
                throw new NotImplementedException();
            }

            public IXLCell CellBelow(int step)
            {
                throw new NotImplementedException();
            }

            public IXLCell CellLeft()
            {
                return _cell.CellLeft();
            }

            public IXLCell CellLeft(int step)
            {
                return _cell.CellLeft(step);
            }

            public IXLCell CellRight()
            {
                return _cell.CellRight();
            }

            public IXLCell CellRight(int step)
            {
                return _cell.CellRight(step);
            }

            public IXLColumn WorksheetColumn()
            {
                return _cell.WorksheetColumn();
            }

            public IXLRow WorksheetRow()
            {
                return _cell.WorksheetRow();
            }

            public IXLConditionalFormat AddConditionalFormat()
            {
                return _cell.AddConditionalFormat();
            }

            public void Select()
            {
                _cell.Select();
            }

            public IXLCell SetActive(bool value = true)
            {
                return _cell.SetActive(value);
            }

            public object Value { get; set; }
            public IXLAddress Address { get; private set; }
            public XLCellValues DataType { get; set; }
            public bool HasHyperlink { get; private set; }
            public string FormulaA1 { get; set; }
            public string FormulaR1C1 { get; set; }
            public IXLStyle Style { get; set; }
            public bool ShareString { get; set; }
            public XLHyperlink Hyperlink { get; set; }
            public IXLWorksheet Worksheet { get; private set; }
            public IXLDataValidation DataValidation { get; private set; }
            public IXLDataValidation NewDataValidation { get; private set; }
            public string ValueCached { get; private set; }
            public IXLRichText RichText { get; private set; }
            public bool HasRichText { get; private set; }
            public IXLComment Comment { get; private set; }
            public bool HasComment { get; private set; }
            public bool HasDataValidation { get; private set; }
            public bool Active { get; set; }
            public bool HasFormula { get; private set; }
            public IXLRangeAddress FormulaReference { get; set; }
        }

    }
}
