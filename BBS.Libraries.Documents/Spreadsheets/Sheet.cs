﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Sheet : IXLWorksheet
    {
      public Sheet(IXLWorksheet sheet)
      {
        
      }

      public void Dispose()
      {
        
      }

      public IXLCells Cells()
      {
        throw new NotImplementedException();
      }

      public IXLCells Cells(string cells)
      {
        throw new NotImplementedException();
      }

      public IXLCells Cells(Func<IXLCell, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public IXLCells CellsUsed()
      {
        throw new NotImplementedException();
      }

      public IXLCells CellsUsed(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLCells CellsUsed(Func<IXLCell, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public IXLCells CellsUsed(bool includeFormats, Func<IXLCell, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public IXLCell FirstCell()
      {
        throw new NotImplementedException();
      }

      public IXLCell FirstCellUsed()
      {
        throw new NotImplementedException();
      }

      public IXLCell FirstCellUsed(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLCell FirstCellUsed(Func<IXLCell, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public IXLCell FirstCellUsed(bool includeFormats, Func<IXLCell, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public IXLCell LastCell()
      {
        throw new NotImplementedException();
      }

      public IXLCell LastCellUsed()
      {
        throw new NotImplementedException();
      }

      public IXLCell LastCellUsed(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLCell LastCellUsed(Func<IXLCell, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public IXLCell LastCellUsed(bool includeFormats, Func<IXLCell, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public bool Contains(string rangeAddress)
      {
        throw new NotImplementedException();
      }

      public bool Contains(IXLRangeBase range)
      {
        throw new NotImplementedException();
      }

      public bool Contains(IXLCell cell)
      {
        throw new NotImplementedException();
      }

      public bool Intersects(string rangeAddress)
      {
        throw new NotImplementedException();
      }

      public bool Intersects(IXLRangeBase range)
      {
        throw new NotImplementedException();
      }

      public IXLRange Unmerge()
      {
        throw new NotImplementedException();
      }

      public IXLRange Merge()
      {
        throw new NotImplementedException();
      }

        public IXLRange Merge(bool checkIntersect)
        {
            throw new NotImplementedException();
        }

        public IXLRange AddToNamed(string rangeName)
      {
        throw new NotImplementedException();
      }

      public IXLRange AddToNamed(string rangeName, XLScope scope)
      {
        throw new NotImplementedException();
      }

      public IXLRange AddToNamed(string rangeName, XLScope scope, string comment)
      {
        throw new NotImplementedException();
      }

      public IXLRangeBase Clear(XLClearOptions clearOptions = XLClearOptions.ContentsAndFormats)
      {
        throw new NotImplementedException();
      }

      public void DeleteComments()
      {
        throw new NotImplementedException();
      }

      public IXLRangeBase SetValue<T>(T value)
      {
        throw new NotImplementedException();
      }

      public IXLRange AsRange()
      {
        throw new NotImplementedException();
      }

      public bool IsMerged()
      {
        throw new NotImplementedException();
      }

      public bool IsEmpty()
      {
        throw new NotImplementedException();
      }

      public bool IsEmpty(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLPivotTable CreatePivotTable(IXLCell targetCell)
      {
        throw new NotImplementedException();
      }

      public IXLPivotTable CreatePivotTable(IXLCell targetCell, string name)
      {
        throw new NotImplementedException();
      }

      public IXLAutoFilter SetAutoFilter()
      {
        throw new NotImplementedException();
      }

      public IXLDataValidation SetDataValidation()
      {
        throw new NotImplementedException();
      }

      public IXLConditionalFormat AddConditionalFormat()
      {
        throw new NotImplementedException();
      }

      public void Select()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet Worksheet { get; private set; }
      public IXLRangeAddress RangeAddress { get; private set; }
      public object Value { set; private get; }
      public XLCellValues DataType { set; private get; }
      public string FormulaA1 { set; private get; }
      public string FormulaR1C1 { set; private get; }
      public IXLStyle Style { get; set; }
      public bool ShareString { set; private get; }
      public IXLHyperlinks Hyperlinks { get; private set; }
      public IXLRow FirstRow()
      {
        throw new NotImplementedException();
      }

      public IXLRow FirstRowUsed()
      {
        throw new NotImplementedException();
      }

      public IXLRow FirstRowUsed(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLRow LastRow()
      {
        throw new NotImplementedException();
      }

      public IXLRow LastRowUsed()
      {
        throw new NotImplementedException();
      }

      public IXLRow LastRowUsed(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLColumn FirstColumn()
      {
        throw new NotImplementedException();
      }

      public IXLColumn FirstColumnUsed()
      {
        throw new NotImplementedException();
      }

      public IXLColumn FirstColumnUsed(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLColumn LastColumn()
      {
        throw new NotImplementedException();
      }

      public IXLColumn LastColumnUsed()
      {
        throw new NotImplementedException();
      }

      public IXLColumn LastColumnUsed(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLColumns Columns()
      {
        throw new NotImplementedException();
      }

      public IXLColumns Columns(string columns)
      {
        throw new NotImplementedException();
      }

      public IXLColumns Columns(string firstColumn, string lastColumn)
      {
        throw new NotImplementedException();
      }

      public IXLColumns Columns(int firstColumn, int lastColumn)
      {
        throw new NotImplementedException();
      }

      public IXLRows Rows()
      {
        throw new NotImplementedException();
      }

      public IXLRows Rows(string rows)
      {
        throw new NotImplementedException();
      }

      public IXLRows Rows(int firstRow, int lastRow)
      {
        throw new NotImplementedException();
      }

      public IXLRow Row(int row)
      {
        throw new NotImplementedException();
      }

      public IXLColumn Column(int column)
      {
        throw new NotImplementedException();
      }

      public IXLColumn Column(string column)
      {
        throw new NotImplementedException();
      }

      public IXLCell Cell(int row, int column)
      {
        throw new NotImplementedException();
      }

      public IXLCell Cell(string cellAddressInRange)
      {
        throw new NotImplementedException();
      }

      public IXLCell Cell(int row, string column)
      {
        throw new NotImplementedException();
      }

      public IXLCell Cell(IXLAddress cellAddressInRange)
      {
        throw new NotImplementedException();
      }

      public IXLRange Range(IXLRangeAddress rangeAddress)
      {
        throw new NotImplementedException();
      }

      public IXLRange Range(string rangeAddress)
      {
        throw new NotImplementedException();
      }

      public IXLRange Range(IXLCell firstCell, IXLCell lastCell)
      {
        throw new NotImplementedException();
      }

      public IXLRange Range(string firstCellAddress, string lastCellAddress)
      {
        throw new NotImplementedException();
      }

      public IXLRange Range(IXLAddress firstCellAddress, IXLAddress lastCellAddress)
      {
        throw new NotImplementedException();
      }

      public IXLRanges Ranges(string ranges)
      {
        throw new NotImplementedException();
      }

      public IXLRange Range(int firstCellRow, int firstCellColumn, int lastCellRow, int lastCellColumn)
      {
        throw new NotImplementedException();
      }

      public int RowCount()
      {
        throw new NotImplementedException();
      }

      public int ColumnCount()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet CollapseRows()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet CollapseColumns()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet ExpandRows()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet ExpandColumns()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet CollapseRows(int outlineLevel)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet CollapseColumns(int outlineLevel)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet ExpandRows(int outlineLevel)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet ExpandColumns(int outlineLevel)
      {
        throw new NotImplementedException();
      }

      public void Delete()
      {
        throw new NotImplementedException();
      }

      public IXLNamedRange NamedRange(string rangeName)
      {
        throw new NotImplementedException();
      }

      public IXLTable Table(int index)
      {
        throw new NotImplementedException();
      }

      public IXLTable Table(string name)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet CopyTo(string newSheetName)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet CopyTo(string newSheetName, int position)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet CopyTo(XLWorkbook workbook, string newSheetName)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet CopyTo(XLWorkbook workbook, string newSheetName, int position)
      {
        throw new NotImplementedException();
      }

      public IXLRange RangeUsed()
      {
        throw new NotImplementedException();
      }

      public IXLRange RangeUsed(bool includeFormats)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet Hide()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet Unhide()
      {
        throw new NotImplementedException();
      }

      public IXLSheetProtection Protect()
      {
        throw new NotImplementedException();
      }

      public IXLSheetProtection Protect(string password)
      {
        throw new NotImplementedException();
      }

      public IXLSheetProtection Unprotect()
      {
        throw new NotImplementedException();
      }

      public IXLSheetProtection Unprotect(string password)
      {
        throw new NotImplementedException();
      }

      public IXLRange Sort()
      {
        throw new NotImplementedException();
      }

      public IXLRange Sort(string columnsToSortBy, XLSortOrder sortOrder = XLSortOrder.Ascending, bool matchCase = false,
        bool ignoreBlanks = true)
      {
        throw new NotImplementedException();
      }

      public IXLRange Sort(int columnToSortBy, XLSortOrder sortOrder = XLSortOrder.Ascending, bool matchCase = false, bool ignoreBlanks = true)
      {
        throw new NotImplementedException();
      }

      public IXLRange SortLeftToRight(XLSortOrder sortOrder = XLSortOrder.Ascending, bool matchCase = false, bool ignoreBlanks = true)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowFormulas()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowFormulas(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowGridLines()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowGridLines(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowOutlineSymbols()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowOutlineSymbols(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowRowColHeaders()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowRowColHeaders(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowRuler()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowRuler(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowWhiteSpace()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowWhiteSpace(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowZeros()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetShowZeros(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetTabColor(XLColor color)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetTabSelected()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetTabSelected(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetTabActive()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetTabActive(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLPivotTable PivotTable(string name)
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetRightToLeft()
      {
        throw new NotImplementedException();
      }

      public IXLWorksheet SetRightToLeft(bool value)
      {
        throw new NotImplementedException();
      }

      public IXLRows RowsUsed(bool includeFormats = false, Func<IXLRow, bool> predicate = null)
      {
        throw new NotImplementedException();
      }

      public IXLRows RowsUsed(Func<IXLRow, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public IXLColumns ColumnsUsed(bool includeFormats = false, Func<IXLColumn, bool> predicate = null)
      {
        throw new NotImplementedException();
      }

      public IXLColumns ColumnsUsed(Func<IXLColumn, bool> predicate)
      {
        throw new NotImplementedException();
      }

      public object Evaluate(string expression)
      {
        throw new NotImplementedException();
      }

      public XLWorkbook Workbook { get; private set; }
      public double ColumnWidth { get; set; }
      public double RowHeight { get; set; }
      public string Name { get; set; }
      public int Position { get; set; }
      public IXLPageSetup PageSetup { get; private set; }
      public IXLOutline Outline { get; private set; }
      public IXLNamedRanges NamedRanges { get; private set; }
      public IXLSheetView SheetView { get; private set; }
      public IXLTables Tables { get; private set; }
      public IXLDataValidations DataValidations { get; private set; }
      public XLWorksheetVisibility Visibility { get; set; }
      public IXLSheetProtection Protection { get; private set; }
      public IXLSortElements SortRows { get; private set; }
      public IXLSortElements SortColumns { get; private set; }
      public bool ShowFormulas { get; set; }
      public bool ShowGridLines { get; set; }
      public bool ShowOutlineSymbols { get; set; }
      public bool ShowRowColHeaders { get; set; }
      public bool ShowRuler { get; set; }
      public bool ShowWhiteSpace { get; set; }
      public bool ShowZeros { get; set; }
      public XLColor TabColor { get; set; }
      public bool TabSelected { get; set; }
      public bool TabActive { get; set; }
      public IXLPivotTables PivotTables { get; private set; }
      public bool RightToLeft { get; set; }
      public IXLBaseAutoFilter AutoFilter { get; private set; }
      public IXLRanges MergedRanges { get; private set; }
      public IXLConditionalFormats ConditionalFormats { get; private set; }
      public IXLRanges SelectedRanges { get; private set; }
      public IXLCell ActiveCell { get; set; }
      public string Author { get; set; }
    }
  }
}
