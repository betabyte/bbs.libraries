﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Alignment 
    {
      public AlignmentHorizontal Horizontal { get; set; }
      public AligntmentVertical Vertical { get; set; }
      public int Indent { get; set; }
      public bool JustifyLastLine { get; set; }
      public AlignmentReadingOrder ReadingOrder { get; set; }
      public int RelativeIndent { get; set; }
      public bool ShrinkToFit { get; set; }
      public int TextRotation { get; set; }
      public bool WrapText { get; set; }
      public bool TopToBottom { get; set; }
  

      public Alignment()
      {
        Horizontal = AlignmentHorizontal.Left;
        Vertical = AligntmentVertical.Bottom;
        ReadingOrder = AlignmentReadingOrder.LeftToRight;
        Indent = 0;
        JustifyLastLine = false;
        RelativeIndent = 0;
        ShrinkToFit = false;
        TextRotation = 0;
        WrapText = false;
        TopToBottom = false;
      }
    }
  }
}
