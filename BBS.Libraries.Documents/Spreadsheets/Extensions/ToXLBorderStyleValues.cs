﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    public static XLBorderStyleValues ToXLBorderStyleValues(this Documents.Spreadsheets.BorderStyle valueStyle)
    {
      switch (valueStyle)
      {
        case Spreadsheets.BorderStyle.DashDot:
          return XLBorderStyleValues.DashDot;
        case Spreadsheets.BorderStyle.DashDotDot:
          return XLBorderStyleValues.DashDotDot;
        case Spreadsheets.BorderStyle.Dashed:
          return XLBorderStyleValues.Dashed;
        case Spreadsheets.BorderStyle.Dotted:
          return XLBorderStyleValues.Dotted;
        case Spreadsheets.BorderStyle.Double:
          return XLBorderStyleValues.Double;
        case Spreadsheets.BorderStyle.Hair:
          return XLBorderStyleValues.Hair;
        case Spreadsheets.BorderStyle.Medium:
          return XLBorderStyleValues.Medium;
        case Spreadsheets.BorderStyle.MediumDashDot:
          return XLBorderStyleValues.MediumDashDot;
        case Spreadsheets.BorderStyle.MediumDashDotDot:
          return XLBorderStyleValues.MediumDashDotDot;
        case Spreadsheets.BorderStyle.MediumDashed:
          return XLBorderStyleValues.MediumDashed;
        case Spreadsheets.BorderStyle.None:
          return XLBorderStyleValues.None;
        case Spreadsheets.BorderStyle.SlantDashDot:
          return XLBorderStyleValues.SlantDashDot;
        case Spreadsheets.BorderStyle.Thick:
          return XLBorderStyleValues.Thick;
        case Spreadsheets.BorderStyle.Thin:
          return XLBorderStyleValues.Thin;
        default:
          return XLBorderStyleValues.None;
      }
    }
  }
}
