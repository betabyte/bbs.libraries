﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    public static XLFontVerticalTextAlignmentValues ToXlFontVerticalTextAlignmentValues(this Spreadsheets.FontVerticalTextAlignment valueAlignment)
    {
      switch (valueAlignment)
      {
        case Spreadsheets.FontVerticalTextAlignment.Baseline:
          return XLFontVerticalTextAlignmentValues.Baseline;
        case Spreadsheets.FontVerticalTextAlignment.Subscript:
          return XLFontVerticalTextAlignmentValues.Subscript;
        case Spreadsheets.FontVerticalTextAlignment.Superscript:
          return XLFontVerticalTextAlignmentValues.Superscript;
        default:
          return XLFontVerticalTextAlignmentValues.Baseline;
      }
    }
  }
}
