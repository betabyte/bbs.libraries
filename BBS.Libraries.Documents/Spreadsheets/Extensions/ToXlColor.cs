﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    public static XLColor ToXlColor(this System.Drawing.Color value)
    {
      switch (value.ToKnownColor())
      {
        case KnownColor.AliceBlue:
          return XLColor.AliceBlue;
        case KnownColor.AntiqueWhite:
          return XLColor.AntiqueWhite;
        case KnownColor.Aqua:
          return XLColor.Aqua;
        case KnownColor.Aquamarine:
          return XLColor.Aquamarine;
        case KnownColor.Blue:
          return XLColor.Blue;
        case KnownColor.Red:
          return XLColor.Red;
        case KnownColor.White:
          return XLColor.White;
        case KnownColor.Black:
          return XLColor.Black;
        case KnownColor.DarkBlue:
          return XLColor.DarkBlue;
        default:
          throw new ArgumentException(string.Format("Color: {0} not implemented", value));
      }
    }
  }
}
