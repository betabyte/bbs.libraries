﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    public static XLAlignmentHorizontalValues ToXLAlignmentHorizontalValues(this Documents.Spreadsheets.AlignmentHorizontal valueHorizontal)
    {
      switch (valueHorizontal)
      {
        case Spreadsheets.AlignmentHorizontal.Center:
          return XLAlignmentHorizontalValues.Center;
        case Spreadsheets.AlignmentHorizontal.CenterContinuous:
          return XLAlignmentHorizontalValues.CenterContinuous;
        case Spreadsheets.AlignmentHorizontal.Distributed:
          return XLAlignmentHorizontalValues.Distributed;
        case Spreadsheets.AlignmentHorizontal.Fill:
          return XLAlignmentHorizontalValues.Fill;
        case Spreadsheets.AlignmentHorizontal.General:
          return XLAlignmentHorizontalValues.General;
        case Spreadsheets.AlignmentHorizontal.Justify:
          return XLAlignmentHorizontalValues.Justify;
        case Spreadsheets.AlignmentHorizontal.Left:
          return XLAlignmentHorizontalValues.Left;
        case Spreadsheets.AlignmentHorizontal.Right:
          return XLAlignmentHorizontalValues.Right;
        default:
          return XLAlignmentHorizontalValues.Left;
      }
    }
  }
}
