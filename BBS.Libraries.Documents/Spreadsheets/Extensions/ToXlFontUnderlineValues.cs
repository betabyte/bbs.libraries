﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    public static XLFontUnderlineValues ToXlFontUnderlineValues(this Spreadsheets.FontUnderLines value)
    {
      switch (value)
      {
        case Spreadsheets.FontUnderLines.Double:
          return XLFontUnderlineValues.Double;
        case Spreadsheets.FontUnderLines.DoubleAccounting:
          return XLFontUnderlineValues.DoubleAccounting;
        case Spreadsheets.FontUnderLines.None:
          return XLFontUnderlineValues.None;
        case Spreadsheets.FontUnderLines.Single:
          return XLFontUnderlineValues.Single;
        case Spreadsheets.FontUnderLines.SingleAccounting:
          return XLFontUnderlineValues.SingleAccounting;
        default:
          return XLFontUnderlineValues.None;
      }
    }
  }
}
