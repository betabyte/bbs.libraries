﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    public static XLAlignmentReadingOrderValues ToXLAlignmentReadingOrderValues(this Documents.Spreadsheets.AlignmentReadingOrder valueOrder)
    {
      switch (valueOrder)
      {
        case Spreadsheets.AlignmentReadingOrder.ContextDependent:
          return XLAlignmentReadingOrderValues.ContextDependent;
        case Spreadsheets.AlignmentReadingOrder.LeftToRight:
          return XLAlignmentReadingOrderValues.LeftToRight;
        case Spreadsheets.AlignmentReadingOrder.RightToLeft:
          return XLAlignmentReadingOrderValues.RightToLeft;
        default:
          return XLAlignmentReadingOrderValues.LeftToRight;
      }
    }
  }
}
