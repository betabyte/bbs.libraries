﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    public static XLFontFamilyNumberingValues ToXlFontFamilyNumberingValues(this Spreadsheets.FontFamilyNumbering value)
    {
      switch (value)
      {
        case Spreadsheets.FontFamilyNumbering.NotApplicable:
          return XLFontFamilyNumberingValues.NotApplicable;
        case Spreadsheets.FontFamilyNumbering.Roman:
          return XLFontFamilyNumberingValues.Roman;
        case Spreadsheets.FontFamilyNumbering.Swiss:
          return XLFontFamilyNumberingValues.Swiss;
        case Spreadsheets.FontFamilyNumbering.Modern:
          return XLFontFamilyNumberingValues.Modern;
        case Spreadsheets.FontFamilyNumbering.Script:
          return XLFontFamilyNumberingValues.Script;
        case Spreadsheets.FontFamilyNumbering.Decorative:
          return XLFontFamilyNumberingValues.Decorative;
        default:
          return XLFontFamilyNumberingValues.NotApplicable;
      }
    }
  }
}
