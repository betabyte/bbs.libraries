﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    //DarkDown,
    //  DarkGray,
    //  DarkGrid,
    //  DarkHorizontal,
    //  DarkTrellis,
    //  DarkUp,
    //  DarkVertical,
    //  Gray0625,
    //  Gray125,
    //  LightDown,
    //  LightGray,
    //  LightGrid,
    //  LightHorizontal,
    //  LightTrellis,
    //  LightUp,
    //  LightVertical,
    //  MediumGray,
    //  None,
    //  Solid,

    public static XLFillPatternValues ToXLFillPatternValues(this Documents.Spreadsheets.FillPattern valuePattern)
    {
      switch (valuePattern)
      {
        case Spreadsheets.FillPattern.DarkDown:
          return XLFillPatternValues.DarkDown;
        case Spreadsheets.FillPattern.DarkGray:
          return XLFillPatternValues.DarkGray;
        case Spreadsheets.FillPattern.DarkGrid:
          return XLFillPatternValues.DarkGrid;
        case Spreadsheets.FillPattern.DarkHorizontal:
          return XLFillPatternValues.DarkHorizontal;
        case Spreadsheets.FillPattern.DarkTrellis:
          return XLFillPatternValues.DarkTrellis;
        case Spreadsheets.FillPattern.DarkUp:
          return XLFillPatternValues.DarkUp;
        case Spreadsheets.FillPattern.DarkVertical:
          return XLFillPatternValues.DarkVertical;
        case Spreadsheets.FillPattern.Gray0625:
          return XLFillPatternValues.Gray0625;
        case Spreadsheets.FillPattern.Gray125:
          return XLFillPatternValues.Gray125;
        case Spreadsheets.FillPattern.LightDown:
          return XLFillPatternValues.LightDown;
        case Spreadsheets.FillPattern.LightGray:
          return XLFillPatternValues.LightGray;
        case Spreadsheets.FillPattern.LightGrid:
          return XLFillPatternValues.LightGrid;
        case Spreadsheets.FillPattern.LightHorizontal:
          return XLFillPatternValues.LightHorizontal;
        case Spreadsheets.FillPattern.LightTrellis:
          return XLFillPatternValues.LightTrellis;
        case Spreadsheets.FillPattern.LightVertical:
          return XLFillPatternValues.LightVertical;
        case Spreadsheets.FillPattern.MediumGray:
          return XLFillPatternValues.MediumGray;
        case Spreadsheets.FillPattern.None:
          return XLFillPatternValues.None;
        case Spreadsheets.FillPattern.Solid:
          return XLFillPatternValues.Solid;
        default:
          return XLFillPatternValues.None;
      }
    }
  }
}
