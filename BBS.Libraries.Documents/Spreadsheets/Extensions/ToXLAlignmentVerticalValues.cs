﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public static partial class Extensions
  {
    public static XLAlignmentVerticalValues ToXLAlignmentVerticalValues(this Documents.Spreadsheets.AligntmentVertical valueVertical)
    {
      switch (valueVertical)
      {
        case Spreadsheets.AligntmentVertical.Bottom:
          return XLAlignmentVerticalValues.Bottom;
        case Spreadsheets.AligntmentVertical.Center:
          return XLAlignmentVerticalValues.Center;
        case Spreadsheets.AligntmentVertical.Distributed:
          return XLAlignmentVerticalValues.Distributed;
        case Spreadsheets.AligntmentVertical.Justify:
          return XLAlignmentVerticalValues.Justify;
        case Spreadsheets.AligntmentVertical.Top:
          return XLAlignmentVerticalValues.Top;
        default:
          return XLAlignmentVerticalValues.Bottom;
      }
    }
  }
}
