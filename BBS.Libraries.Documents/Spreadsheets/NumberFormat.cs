﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class CellFormat
    {
      private string _format;

      public int? CellFormatId { get; set; }

      public string Format
      {
        get
        {
          return string.IsNullOrWhiteSpace(_format) ? string.Empty : _format;
        }
        set { _format = value; }
      }
    }
  }
}
