﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public enum BorderStyle
    {
      DashDot,
      DashDotDot,
      Dashed,
      Dotted,
      Double,
      Hair,
      Medium,
      MediumDashDot,
      MediumDashDotDot,
      MediumDashed,
      None,
      SlantDashDot,
      Thick,
      Thin,
    }
  }
}
