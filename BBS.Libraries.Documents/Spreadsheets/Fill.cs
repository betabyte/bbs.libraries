﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Fill 
    {
      public System.Drawing.Color BackgroundColor { get; set; }
      public System.Drawing.Color PatternColor { get; set; }
      public System.Drawing.Color PatternBackgroundColor { get; set; }
      public FillPattern PatternType { get; set; }

      public Fill()
      {
        PatternType = FillPattern.None;
        BackgroundColor = System.Drawing.Color.White;
        PatternColor = System.Drawing.Color.Black;
        PatternBackgroundColor = System.Drawing.Color.White;
      }
    }
  }
}
