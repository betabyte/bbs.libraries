﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public enum AlignmentHorizontal
    {
      Center,
      CenterContinuous,
      Distributed,
      Fill,
      General,
      Justify,
      Left,
      Right,
    }
  }
}
