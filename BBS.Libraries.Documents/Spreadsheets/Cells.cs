﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Cells : ClosedXML.Excel.IXLCells
    {     
      private List<Cell> _cellList { get; set; }

      private IXLWorksheet _sheet { get; set; }

      public Cell this[int row, int column]
      {
        get
        {
          row = row + 1;
          column = column + 1;

          return new Cell(_sheet.Cell(row, column));
        }
        set
        {
          row = row + 1;
          column = column + 1;

          var c = new Cell(_sheet.Cell(row, column));
          
          value = c ;
        }
      }

      public Cells (IXLWorksheet worksheet)
      {
        _cellList = new List<Cell>();
        _sheet = worksheet;
      }

      public IEnumerator<IXLCell> GetEnumerator()
      {
        throw new NotImplementedException();
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return GetEnumerator();
      }

      public IXLCells SetDataType(XLCellValues dataType)
      {
        throw new NotImplementedException();
      }

      public IXLCells Clear(XLClearOptions clearOptions = XLClearOptions.ContentsAndFormats)
      {
        throw new NotImplementedException();
      }

      public void DeleteComments()
      {
        throw new NotImplementedException();
      }

      public void Select()
      {
        throw new NotImplementedException();
      }

      public object Value { set; private get; }
      public XLCellValues DataType { set; private get; }
      public string FormulaA1 { set; private get; }
      public string FormulaR1C1 { set; private get; }
      public IXLStyle Style { get; set; }
    }
  }
}
