﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Workbook : XLWorkbook
    {
      public Cells Cells { get; set; }

      public IXLWorksheet Sheet { get; set; }

      public void SaveAs(string fileName, bool overwriteExisting = false)
      {
        if (!overwriteExisting)
        {
          fileName = BBS.Libraries.IO.File.FileNameIncrementor(fileName);
        }

        base.SaveAs(fileName);
      }

      public void SaveAs(System.IO.Stream stream)
      {
        base.SaveAs(stream);
      }

      public Workbook()
      {
        
      }

      public IXLWorksheet AddWorksheet(string workbookName)
      {
        Sheet = base.AddWorksheet(workbookName);
        Cells = new Cells(Sheet);
        Cells.Value = Sheet.Cells();
        return Sheet;
      }
    }
  }
}
