﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Border 
    {
      public BorderStyle OutsideBorder { set; private get; }
      public System.Drawing.Color OutsideBorderColor { set; private get; }
      public BorderStyle InsideBorder { set; private get; }
      public System.Drawing.Color InsideBorderColor { set; private get; }
      public BorderStyle LeftBorder { get; set; }
      public System.Drawing.Color LeftBorderColor { get; set; }
      public BorderStyle RightBorder { get; set; }
      public System.Drawing.Color RightBorderColor { get; set; }
      public BorderStyle TopBorder { get; set; }
      public System.Drawing.Color TopBorderColor { get; set; }
      public BorderStyle BottomBorder { get; set; }
      public System.Drawing.Color BottomBorderColor { get; set; }
      public bool DiagonalUp { get; set; }
      public bool DiagonalDown { get; set; }
      public BorderStyle DiagonalBorder { get; set; }
      public System.Drawing.Color DiagonalBorderColor { get; set; }

      public Border()
      {
        OutsideBorder = BorderStyle.None;
        InsideBorder = BorderStyle.None;
        LeftBorder = BorderStyle.None;
        RightBorder = BorderStyle.None;
        TopBorder = BorderStyle.None;
        BottomBorder = BorderStyle.None;
        DiagonalBorder = BorderStyle.None;

        OutsideBorderColor = System.Drawing.Color.Black;
        InsideBorderColor = System.Drawing.Color.Black;
        LeftBorderColor = System.Drawing.Color.Black;
        RightBorderColor = System.Drawing.Color.Black;
        TopBorderColor = System.Drawing.Color.Black;
        BottomBorderColor = System.Drawing.Color.Black;
        DiagonalBorderColor = System.Drawing.Color.Black;
        
        DiagonalUp = false;
        DiagonalDown = false;
      }
    }
  }
}
