﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Font
    {
      public bool Bold { get; set; }
      public bool Italic { get; set; }
      public FontUnderLines Underline { get; set; }
      public bool Strikethrough { get; set; }
      public FontVerticalTextAlignment VerticalAlignment { get; set; }
      public bool Shadow { get; set; }
      public double Size { get; set; }
      public System.Drawing.Color Color { get; set; }
      public string FontName { get; set; }
      public FontFamilyNumbering FamilyNumbering { get; set; }

      public Font()
      {
        // Default values
        Bold = false;
        Italic = false;
        Underline = FontUnderLines.None;
        Strikethrough = false;
        VerticalAlignment = FontVerticalTextAlignment.Baseline;
        Shadow = false;
        Size = 11d;
        Color = System.Drawing.Color.Black;
        FontName = "Arial";
        FamilyNumbering = FontFamilyNumbering.NotApplicable;
      }
    }
  }
}
