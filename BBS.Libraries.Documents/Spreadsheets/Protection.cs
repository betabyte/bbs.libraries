﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets
  {
    public class Protection
    {
      /// <summary>
      /// Locked by default
      /// </summary>
      public bool Locked { get; set; }
      public bool Hidden { get; set; }

      public Protection()
      {
        Locked = true;
        Hidden = false;
      }
    }
  }
}
