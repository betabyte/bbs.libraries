﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBS.Libraries.Documents
{
  public partial class Spreadsheets: IDisposable
  {
    public Workbook Book { get; set; }

    public Spreadsheets()
    {
      Book = new Workbook();
    }

    public void Dispose()
    {
      Book.Dispose();
    }
  }
}
